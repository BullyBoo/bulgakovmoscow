package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.content.Intent;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.adapters.RouteListAdapter;
import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Активити "Маршруты"
 */
public class RoutesActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener {

    private TextView places_text, routes_text, search_text, info_text;
    private RelativeLayout placesLayout, routesLayout, searchLayout, infoLayout;

    private ListView list;
    private TextView activityTitle;

    private RouteListAdapter adapter;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.routes_activity);

        placesLayout = (RelativeLayout) findViewById(R.id.placesLayout);
        routesLayout = (RelativeLayout) findViewById(R.id.routesLayout);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        infoLayout = (RelativeLayout) findViewById(R.id.infoLayout);
        placesLayout.setOnClickListener(this);
        routesLayout.setOnClickListener(this);
        searchLayout.setOnClickListener(this);
        infoLayout.setOnClickListener(this);

        places_text = (TextView) findViewById(R.id.places_text);
        routes_text = (TextView) findViewById(R.id.routes_text);
        search_text = (TextView) findViewById(R.id.search_text);
        info_text = (TextView) findViewById(R.id.info_text);

        activityTitle = (TextView) findViewById(R.id.activity_title);
        TypeFaceHelper.setDescriptionTypeFace(this, activityTitle);

        list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(this);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this, places_text);
        TypeFaceHelper.setDescriptionTypeFace(this, routes_text);
        TypeFaceHelper.setDescriptionTypeFace(this, search_text);
        TypeFaceHelper.setDescriptionTypeFace(this, info_text);

        List<Route> routes = Route.getAll();

        adapter = new RouteListAdapter(this, routes);

        list.setAdapter(adapter);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.placesLayout:
                intent = new Intent(RoutesActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.routesLayout:
//                уже на этом экране
                break;
            case R.id.searchLayout:
                intent = new Intent(RoutesActivity.this, SearchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            case R.id.infoLayout:
                intent = new Intent(RoutesActivity.this, InfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            default:
                break;
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        updateLocale();
    }

    private void updateLocale(){

        PreferencesHelper helper = new PreferencesHelper(this);

        if(helper.hasLocal())
            new LocaleHelper(this, helper.getLocal());

        places_text.setText(getResources().getString(R.string.places_text));
        routes_text.setText(getResources().getString(R.string.routes_text));
        search_text.setText(getResources().getString(R.string.search_text));
        info_text.setText(getResources().getString(R.string.info_text));
        activityTitle.setText(getResources().getString(R.string.routes_text));
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Route route = (Route) adapter.getItem(position);

        if(route != null){
            Intent intent = new Intent(this, RouteCardActivity.class);
            intent.putExtra("route_id", route.getRouteId());
            startActivity(intent);
            overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
        }
    }
}
