package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.app.FragmentManager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.fragments.info.AboutAppFragment;
import ru.bullyboo.bulgakov.fragments.info.AboutProjectFragment;
import ru.bullyboo.bulgakov.fragments.info.HistoryFragment;
import ru.bullyboo.bulgakov.fragments.info.SettingsFragment;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Активити "Информция"
 */
public class InfoActivity extends Activity implements View.OnClickListener{

    private RelativeLayout historyLayout
            , aboutProjectLayout
            , aboutAppLayout
            , commentsLayout
            , settingsLayout;

    private TextView title
            , histiry
            , aboutProject
            , aboutApp
            , comments
            , settings;

    private TextView places_text, routes_text, search_text, info_text;
    private RelativeLayout placesLayout, routesLayout, searchLayout, infoLayout;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.info_activity);

        title = (TextView) findViewById(R.id.activity_title);
        histiry = (TextView) findViewById(R.id.history);
        aboutProject = (TextView) findViewById(R.id.about_project);
        aboutApp = (TextView) findViewById(R.id.about_app);
        comments = (TextView) findViewById(R.id.write_comment);
        settings = (TextView) findViewById(R.id.settings);

//        меняем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this, title);
        TypeFaceHelper.setDescriptionTypeFace(this, histiry);
        TypeFaceHelper.setDescriptionTypeFace(this, aboutProject);
        TypeFaceHelper.setDescriptionTypeFace(this, aboutApp);
        TypeFaceHelper.setDescriptionTypeFace(this, comments);
        TypeFaceHelper.setDescriptionTypeFace(this, settings);

        historyLayout = (RelativeLayout) findViewById(R.id.historyLayout);
        aboutProjectLayout = (RelativeLayout) findViewById(R.id.about_projectLayout);
        aboutAppLayout = (RelativeLayout) findViewById(R.id.about_appLayout);
        commentsLayout = (RelativeLayout) findViewById(R.id.write_commentLayout);
        settingsLayout = (RelativeLayout) findViewById(R.id.settingsLayout);

        historyLayout.setOnClickListener(choiceListener);
        aboutProjectLayout.setOnClickListener(choiceListener);
        aboutAppLayout.setOnClickListener(choiceListener);
        settingsLayout.setOnClickListener(choiceListener);
        commentsLayout.setOnClickListener(choiceListener);

//        bottomBar
        placesLayout = (RelativeLayout) findViewById(R.id.placesLayout);
        routesLayout = (RelativeLayout) findViewById(R.id.routesLayout);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        infoLayout = (RelativeLayout) findViewById(R.id.infoLayout);
        placesLayout.setOnClickListener(this);
        routesLayout.setOnClickListener(this);
        searchLayout.setOnClickListener(this);
        infoLayout.setOnClickListener(this);

        places_text = (TextView) findViewById(R.id.places_text);
        routes_text = (TextView) findViewById(R.id.routes_text);
        search_text = (TextView) findViewById(R.id.search_text);
        info_text = (TextView) findViewById(R.id.info_text);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this, places_text);
        TypeFaceHelper.setDescriptionTypeFace(this, routes_text);
        TypeFaceHelper.setDescriptionTypeFace(this, search_text);
        TypeFaceHelper.setDescriptionTypeFace(this, info_text);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.placesLayout:
                intent = new Intent(InfoActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.routesLayout:
                intent = new Intent(InfoActivity.this, RoutesActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.searchLayout:
                intent = new Intent(InfoActivity.this, SearchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.infoLayout:
//                уже находимся на этом экране
                break;
            default:
                break;
        }
    }

    View.OnClickListener choiceListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {

            Fragment fragment = null;

            switch (v.getId()){
                case R.id.about_projectLayout:
                    fragment = new AboutProjectFragment();
                    break;
                case R.id.historyLayout:
                    fragment = new HistoryFragment();
                    break;
                case R.id.about_appLayout:
                    fragment = new AboutAppFragment();
                    break;
                case R.id.write_commentLayout:
//                открывает приложение на Google Play маркете
                    Intent goToMarket = new Intent(Intent.ACTION_VIEW);
                    goToMarket.setData(Uri.parse("market://details?id=com.mapbox.mapboxsdk.android.testapp"));
                    startActivity(goToMarket);
                    break;
                case R.id.settingsLayout:
                    fragment = new SettingsFragment();
                    break;
                default:
                    break;
            }
            if(fragment != null){
                getFragmentManager().beginTransaction()
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .add(R.id.frame, fragment)
                        .addToBackStack(null)
                        .commit();
            }
        }
    };

    public void updateLocale(){
        PreferencesHelper helper = new PreferencesHelper(this);
        if(helper.hasLocal())
            new LocaleHelper(this, helper.getLocal());

        title.setText(getResources().getString(R.string.info));
        histiry.setText(getResources().getString(R.string.historical));
        aboutProject.setText(getResources().getString(R.string.about_project));
        aboutApp.setText(getResources().getString(R.string.about_app));
        comments.setText(getResources().getString(R.string.write_comment));
        settings.setText(getResources().getString(R.string.settings));

        places_text.setText(getResources().getString(R.string.places_text));
        routes_text.setText(getResources().getString(R.string.routes_text));
        search_text.setText(getResources().getString(R.string.search_text));
        info_text.setText(getResources().getString(R.string.info_text));


    }
}
