package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.viewpagerindicator.CirclePageIndicator;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.adapters.ViewPagerAdapter;
import ru.bullyboo.bulgakov.data.place.PicturesPlace;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.PlaceJson;
import ru.bullyboo.bulgakov.data.route.RouteJson;
import ru.bullyboo.bulgakov.interfaces.FirstStartState;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.ShareHelper;
import ru.bullyboo.bulgakov.tools.StartSettingsAppChecker;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;
import ru.bullyboo.bulgakov.ui.SharingDialog;

/**
 * Данный экран выводит всю информацию о выбранном объекте Place,
 * отображет изображения, если они есть.
 *
 * Так же на этом экране возможно воспольоваться кнопкой "Поделиться"
 * с друзьями на Facebook и отметить место как пройденное
 *
 */
public class MarkerActivity extends Activity implements View.OnClickListener, FirstStartState {

    private TextView title, description, address
            , moreText, showOnMapText, shareFacebookText, activityTitle;

    private ViewPager pager;
    private CirclePageIndicator indicator;

    private ImageView back;
    private ImageView onMap;
    private RelativeLayout relativeLayout;
    private LinearLayout scrollLinear, button_ll;
    private PreferencesHelper helper;
    private RelativeLayout shareFacebook, more, showOnMap;
    private Place place;

    private TextView places_text, routes_text, search_text, info_text;
    private RelativeLayout placesLayout, routesLayout, searchLayout, infoLayout;

    private List<PicturesPlace> pictures;

    private String title_name;
    private int mainId = 0;
    private int image_number = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.marker_activity);
        FacebookSdk.sdkInitialize(this.getApplicationContext());

        helper = new PreferencesHelper(this);

        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        address = (TextView) findViewById(R.id.address);
        moreText = (TextView) findViewById(R.id.textView3);
        showOnMapText = (TextView) findViewById(R.id.textView4);
        shareFacebookText = (TextView) findViewById(R.id.textView5);
        activityTitle = (TextView) findViewById(R.id.activity_title);

        pager = (ViewPager) findViewById(R.id.viewPager);

        indicator = (CirclePageIndicator) findViewById(R.id.indicator);

        back = (ImageView) findViewById(R.id.back);
        onMap = (ImageView) findViewById(R.id.on_map);

        shareFacebook = (RelativeLayout) findViewById(R.id.share_facebook);
        more = (RelativeLayout) findViewById(R.id.more);
        showOnMap = (RelativeLayout) findViewById(R.id.show_on_map);

        relativeLayout = (RelativeLayout) findViewById(R.id.relativeLayout);

        scrollLinear = (LinearLayout) findViewById(R.id.scroll_ll);
        button_ll = (LinearLayout) findViewById(R.id.button_ll);

        onMap.setOnClickListener(this);
        back.setOnClickListener(this);
        shareFacebook.setOnClickListener(this);
        more.setOnClickListener(this);
        showOnMap .setOnClickListener(this);

//        bottomBar
        placesLayout = (RelativeLayout) findViewById(R.id.placesLayout);
        routesLayout = (RelativeLayout) findViewById(R.id.routesLayout);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        infoLayout = (RelativeLayout) findViewById(R.id.infoLayout);
        placesLayout.setOnClickListener(bottomBar);
        routesLayout.setOnClickListener(bottomBar);
        searchLayout.setOnClickListener(bottomBar);
        infoLayout.setOnClickListener(bottomBar);

        places_text = (TextView) findViewById(R.id.places_text);
        routes_text = (TextView) findViewById(R.id.routes_text);
        search_text = (TextView) findViewById(R.id.search_text);
        info_text = (TextView) findViewById(R.id.info_text);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this, places_text);
        TypeFaceHelper.setDescriptionTypeFace(this, routes_text);
        TypeFaceHelper.setDescriptionTypeFace(this, search_text);
        TypeFaceHelper.setDescriptionTypeFace(this, info_text);


        if(getIntent().hasExtra(Place.NAME)){
            title_name = getIntent().getExtras().getString(Place.NAME);
        }else{
            Toast.makeText(this,
                    this.getResources().getString(R.string.error_place)
                    , Toast.LENGTH_LONG)
                    .show();
        }

        setTextViews();

    }

    public void setTextViews() {

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
            place = Place.getByPlaceName(title_name);
        }
        if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
            place = Place.getByPlaceNameEn(title_name);
        }

        TypeFaceHelper.setTitleTypeFace(this, title);
        TypeFaceHelper.setDescriptionTypeFace(this, description);
        TypeFaceHelper.setDescriptionTypeFace(this, address);
        TypeFaceHelper.setDescriptionTypeFace(this, activityTitle);

        TypeFaceHelper.setDescriptionTypeFace(this, moreText);
        TypeFaceHelper.setDescriptionTypeFace(this, showOnMapText);
        TypeFaceHelper.setDescriptionTypeFace(this, shareFacebookText);

//            получаем id места и проверяем, что оно не пустое
        mainId = place.getPlaceId();

        if (mainId != 0){
            pictures = PicturesPlace.getByParentId(mainId);

            if (pictures.size() != 0){
                ViewPagerAdapter pagerAdapter = new ViewPagerAdapter(this, this, place, true, true);
                pager.setAdapter(pagerAdapter);
                indicator.setViewPager(pager);
            }else{
//                    если у Place нет изображений, убираем кнопку Big и ViewPager
                scrollLinear.removeView(relativeLayout);
                Toast.makeText(this,
                        getResources().getString(R.string.error_place_pictures),
                        Toast.LENGTH_LONG).show();
            }
        }else{
//                если id не был найден, предлагаем пользователю обновить базу данных
            finish();
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.back:
                finish();
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.more:
                String moreText = "";
                if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                    moreText = place.getPlaceDescription();
                }else if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                    moreText = place.getPlaceDescriptionEn();
                }
                description.setText(description.getText()
                        + "\n\n"
                        + moreText);
                button_ll.removeView(more);
                break;
            case R.id.on_map:
                showPlaceOnMap();
                break;
            case R.id.show_on_map:
                showPlaceOnMap();
                break;
            case R.id.share_facebook:
//                ShareHelper shareHelper = new ShareHelper(this, this);
//                shareHelper.sendShare(shareHelper.createShared(place));
                new SharingDialog(this, this, place);
                break;
            default:
                break;
        }
    }

    private void showPlaceOnMap(){
        if(place != null){
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN))
                helper.savePlaceLocation(place.getPlaceLat(), place.getPlaceLng(), place.getPlaceNameEn());
            else if (helper.getLocal().equals(PreferencesHelper.LOCAL_RU))
                helper.savePlaceLocation(place.getPlaceLat(), place.getPlaceLng(), place.getPlaceName());

            Intent intent = new Intent(MarkerActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.to_right_anim_start, R.anim.to_right_anim_end);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        проверяем, если отправленный запрос не равен 0, то получаем положение ViewPager
//        из ImageActivity, на которой остановился пользователь
        if(requestCode != 0){
            image_number = data.getIntExtra("im_num", 0);
        }
//        устанавливаем полученную позицию
        pager.setCurrentItem(image_number);
    }

    @Override
    public void firstSettingWasEnd() {
//        перезапускаем Activity и передаем ему те же входные данные
        startActivity(new Intent(this, MarkerActivity.class).putExtra(Place.NAME, title_name));
        overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
    }

    @Override
    public void getPermisson() {}

    @Override
    public void onResume() {
        super.onResume();
        updateLocale();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
        overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
    }

    private void updateLocale(){

        if(place != null) {
            PreferencesHelper helper = new PreferencesHelper(this);

            if(helper.hasLocal()){
                new LocaleHelper(this, helper.getLocal());

                activityTitle.setText(getResources().getString(R.string.places));
                moreText.setText(getResources().getString(R.string.more));
                showOnMapText.setText(getResources().getString(R.string.show_on_map));
                shareFacebookText.setText(getResources().getString(R.string.share_facebook));

                places_text.setText(getResources().getString(R.string.places_text));
                routes_text.setText(getResources().getString(R.string.routes_text));
                search_text.setText(getResources().getString(R.string.search_text));
                info_text.setText(getResources().getString(R.string.info_text));

                if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                    title.setText(place.getPlaceName().toUpperCase());
                    description.setText(place.getPlaceTextMain());
                    address.setText(place.getPlaceAddress());
                }
                if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                    title.setText(place.getPlaceNameEn().toUpperCase());
                    description.setText(place.getPlaceTextMainEn());
                    address.setText(place.getPlaceAddressEn());
                }
            }
        } else {
            finish();
        }
    }

    View.OnClickListener bottomBar = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent intent = null;

            switch (v.getId()) {
                case R.id.placesLayout:
                    intent = new Intent(MarkerActivity.this, MainActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                    break;
                case R.id.routesLayout:
                    intent = new Intent(MarkerActivity.this, RoutesActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);;
                    break;
                case R.id.searchLayout:
                    intent = new Intent(MarkerActivity.this, SearchActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                    break;
                case R.id.infoLayout:
                    intent = new Intent(MarkerActivity.this, InfoActivity.class);
                    startActivity(intent);
                    overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                    break;
                default:
                    break;
            }
        }
    };
}
