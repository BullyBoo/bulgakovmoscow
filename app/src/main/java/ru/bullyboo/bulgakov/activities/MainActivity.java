package ru.bullyboo.bulgakov.activities;

import android.Manifest;
import android.app.Activity;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.fragments.ListPlacesFragment;
import ru.bullyboo.bulgakov.fragments.MapFragment;
import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.interfaces.FirstStartState;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.StartSettingsAppChecker;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;
import ru.bullyboo.bulgakov.ui.FirstStartDialog;

/**
 * Основной экран приложения, из которого происходит перемещение
 * по всем экранам и фрагментам приложения.
 *
 * Так же из этого класса запрашиватся разрешения.
 *
 */
public class MainActivity extends Activity implements View.OnClickListener, FirstStartState {

    private LinearLayout topBarButtons;

    private RelativeLayout routeTopBar;
    private TextView activityTitle;
    private ImageView back;

    private ImageView places, routes;
    private TextView places_text, routes_text, search_text, info_text;
    private RelativeLayout placesLayout, routesLayout, searchLayout, infoLayout;

    private Button listButton, mapButton;

    private StartSettingsAppChecker startSettingsAppChecker;

    private PreferencesHelper helper;

    public MapFragment mapFragment;
    public ListPlacesFragment listFragment;

    public FirstStartDialog dialog;

    private static final int ALL_PERMISSONS = 1;

    private static final String LIST = "list";
    private static final String MAP = "map";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        helper = new PreferencesHelper(this);
        if(helper.hasLocal())
            new LocaleHelper(this, helper.getLocal());

        setContentView(R.layout.main);

//        инициализируем фрагменты
        mapFragment = new MapFragment();

        getFragmentManager()
                .beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .add(R.id.frame, mapFragment)
                .commit();

//        bottomBar
        places = (ImageView) findViewById(R.id.places);
        routes = (ImageView) findViewById(R.id.routes);

        placesLayout = (RelativeLayout) findViewById(R.id.placesLayout);
        routesLayout = (RelativeLayout) findViewById(R.id.routesLayout);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        infoLayout = (RelativeLayout) findViewById(R.id.infoLayout);
        placesLayout.setOnClickListener(this);
        routesLayout.setOnClickListener(this);
        searchLayout.setOnClickListener(this);
        infoLayout.setOnClickListener(this);

        places_text = (TextView) findViewById(R.id.places_text);
        routes_text = (TextView) findViewById(R.id.routes_text);
        search_text = (TextView) findViewById(R.id.search_text);
        info_text = (TextView) findViewById(R.id.info_text);

//        topBar
        topBarButtons = (LinearLayout) findViewById(R.id.topBarLinear);

        listButton = (Button) findViewById(R.id.listButton);
        mapButton = (Button) findViewById(R.id.mapButton);

        listButton.setOnClickListener(swipeFragmentListener);
        mapButton.setOnClickListener(swipeFragmentListener);

        activityTitle = (TextView) findViewById(R.id.activity_title);
        TypeFaceHelper.setDescriptionTypeFace(this, activityTitle);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this, places_text);
        TypeFaceHelper.setDescriptionTypeFace(this, routes_text);
        TypeFaceHelper.setDescriptionTypeFace(this, search_text);
        TypeFaceHelper.setDescriptionTypeFace(this, info_text);
        TypeFaceHelper.setDescriptionTypeFace(this, listButton);
        TypeFaceHelper.setDescriptionTypeFace(this, mapButton);

        routeTopBar = (RelativeLayout) findViewById(R.id.routeTopBar);
        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                routeTopBarClose();
            }
        });
        routeTopBar.setVisibility(View.INVISIBLE);

//        вызываем класс, отвечающий за первоначальные настройки

        dialog = new FirstStartDialog(this);

        startSettingsAppChecker =
                new StartSettingsAppChecker(MainActivity.this, MainActivity.this, dialog);
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.placesLayout:
//                уже находимся на этом экране
                break;
            case R.id.routesLayout:
                intent = new Intent(MainActivity.this, RoutesActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            case R.id.searchLayout:
                intent = new Intent(MainActivity.this, SearchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            case R.id.infoLayout:
                intent = new Intent(MainActivity.this, InfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            default:
                break;
        }
    }

    View.OnClickListener swipeFragmentListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            switch (v.getId()) {
                case R.id.listButton:
                    setButtons(LIST);

                    listFragment = new ListPlacesFragment();
                    getFragmentManager()
                            .beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                            .add(R.id.frame, listFragment, "list_places_fragment")
                            .addToBackStack(null)
                            .commit();

                    break;
                case R.id.mapButton:
                    setButtons(MAP);

                    getFragmentManager()
                            .beginTransaction()
                            .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                            .remove(listFragment)
                            .commit();
                    break;
                default:
                    break;
            }
        }
    };

    private void setButtons(String button){
        if(button.equals("list")){
            listButton.setBackgroundResource(R.drawable.list_button);
            listButton.setTextColor(getResources().getColor(R.color.white));
            mapButton.setBackgroundResource(R.drawable.inactive_button_map);
            mapButton.setTextColor(getResources().getColor(R.color.green));
        }
        if(button.equals("map")){
            listButton.setBackgroundResource(R.drawable.inactive_button_list);
            listButton.setTextColor(getResources().getColor(R.color.green));
            mapButton.setBackgroundResource(R.drawable.map_button);
            mapButton.setTextColor(getResources().getColor(R.color.white));
        }
    }

    private boolean checkPermisson() {
        if (Build.VERSION.SDK_INT >= 23) {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                            == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_NETWORK_STATE)
                            == PackageManager.PERMISSION_GRANTED &&
                    ContextCompat.checkSelfPermission(this, Manifest.permission.INTERNET)
                            == PackageManager.PERMISSION_GRANTED) {
                return true;
            } else {
                ActivityCompat.requestPermissions(this,
                        new String[]{
                                Manifest.permission.ACCESS_FINE_LOCATION,
                                Manifest.permission.ACCESS_COARSE_LOCATION,
                                Manifest.permission.ACCESS_NETWORK_STATE,
                                Manifest.permission.INTERNET
                        }, ALL_PERMISSONS);
            }
        }
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();

        countForFinish = 0;

        updateLocale();

        places.setImageResource(R.drawable.places_button_active_large);
        routes.setImageResource(R.drawable.routes_button_large);

        if(helper.hasPlaceLocation()){

            getFragmentManager()
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .replace(R.id.frame, mapFragment)
                    .commit();

            setButtons(MAP);

            Location loc = helper.getPlaceLocation();
            mapFragment.mapHelper.updatePlaceCameraPosition(loc);
            mapFragment.mapHelper.showInfoWindow(helper.getPlaceLocationMarkerTitle());

        } else if(helper.hasRouteId()){

            getFragmentManager()
                    .beginTransaction()
                    .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_CLOSE)
                    .replace(R.id.frame, mapFragment)
                    .commit();

            setButtons(MAP);

            mapFragment.hasARoute = true;
            mapFragment.route_id = helper.getRouteId();

            places.setImageResource(R.drawable.places_button_large);
            routes.setImageResource(R.drawable.routes_button_active_large);

            topBarButtons.setVisibility(View.INVISIBLE);
            routeTopBar.setVisibility(View.VISIBLE);
        }
    }

    private int countForFinish = 0;

    @Override
    public void onBackPressed() {
        ListPlacesFragment fragment
                = (ListPlacesFragment)
                getFragmentManager().findFragmentByTag("list_places_fragment");
        if(fragment != null){
            fragment.fihishListPlacesFragment();
            setButtons(MAP);
        } else {
            countForFinish++;
            if(mapFragment.hasARoute){
                routeTopBarClose();
            }
            if(countForFinish == 1){
                Toast.makeText(this, getResources().getString(R.string.exit), Toast.LENGTH_LONG).show();
            } else if(countForFinish == 2){
                System.exit(0);
            }
        }

    }

    private void routeTopBarClose(){
        mapFragment.hasARoute = false;
        mapFragment.route_id = 0;

        mapFragment.onResume();

        places.setImageResource(R.drawable.places_button_active_large);
        routes.setImageResource(R.drawable.routes_button_large);

        countForFinish--;

        topBarButtons.setVisibility(View.VISIBLE);
        routeTopBar.setVisibility(View.INVISIBLE);
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();

        startSettingsAppChecker.stopAsunc(1);
        startSettingsAppChecker.stopAsunc(2);

        helper.removePlaceLocation();
    }

    private void updateLocale(){
        if(helper.hasLocal())
            new LocaleHelper(this, helper.getLocal());

        places_text.setText(getResources().getString(R.string.places_text));
        routes_text.setText(getResources().getString(R.string.routes_text));
        search_text.setText(getResources().getString(R.string.search_text));
        info_text.setText(getResources().getString(R.string.info_text));
        mapButton.setText(getResources().getString(R.string.map));
        listButton.setText(getResources().getString(R.string.list));
    }

    @Override
    public void firstSettingWasEnd() {
//        первоначальная настройка закончена
        mapFragment.createMap();
    }

    @Override
    public void getPermisson() {
        if(checkPermisson()){
            dialog.setCheckBox5Text(getResources().getString(R.string.check_box51));
            dialog.setCheckBox5Checked(true);
            dialog.setProgreeBar5Visible(false);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == ALL_PERMISSONS) {
            for (int i = 0; i < permissions.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_GRANTED) {
                } else {
                    Toast.makeText(MainActivity.this,
                            getResources().getString(R.string.denied_permissions),
                            Toast.LENGTH_SHORT).show();
                    MainActivity.this.checkPermisson();
                    return;
                }
            }
            dialog.setCheckBox5Text(getResources().getString(R.string.check_box51));
            dialog.setCheckBox5Checked(true);
            dialog.setProgreeBar5Visible(false);
        }
    }

}
