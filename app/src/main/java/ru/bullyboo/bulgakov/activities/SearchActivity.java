package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.adapters.SearchListAdapter;
import ru.bullyboo.bulgakov.asunctasks.CalculateDistance;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.TagsPlace;
import ru.bullyboo.bulgakov.data.place.ThemesPlace;
import ru.bullyboo.bulgakov.interfaces.CalculateDistanceState;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Экран "Поиск"
 */
public class SearchActivity extends Activity implements TextWatcher
        , AdapterView.OnItemClickListener
        , View.OnClickListener
        , CalculateDistanceState{

    private EditText search_edit;
    private ListView list;
    private ImageView clear;
    private TextView search_dis;
    private ProgressBar progress;

    private ScrollView scroll;
    private TextView address, personal, writer, prose, theater;
    private RelativeLayout addressLayout, personalLayout, writerLayout, proseLayout, theaterLayout;

    private List<Place> placesList;
    private SearchListAdapter searchListAdapter;

    private PreferencesHelper helper;

    private TextView places_text, routes_text, search_text, info_text;
    private RelativeLayout placesLayout, routesLayout, searchLayout, infoLayout;

    private CalculateDistance calculateDistance;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.search_activity);

        helper = new PreferencesHelper(this);

        search_edit = (EditText) findViewById(R.id.editText);
        list = (ListView) findViewById(R.id.list);
        clear = (ImageView) findViewById(R.id.clear);

        clear.setOnClickListener(clearListener);
        clear.setVisibility(View.INVISIBLE);

        search_dis = (TextView) findViewById(R.id.search_dis);
        search_dis.setVisibility(View.INVISIBLE);

        progress = (ProgressBar) findViewById(R.id.progressBar);
        progress.setVisibility(View.INVISIBLE);

        list.setOnItemClickListener(this);
        search_edit.addTextChangedListener(this);

        placesList = Place.getAllVisible();
        searchListAdapter = new SearchListAdapter(this, placesList);
        list.setAdapter(searchListAdapter);
        list.setVisibility(View.INVISIBLE);

//        категории
        scroll = (ScrollView) findViewById(R.id.scroll);

        address = (TextView) findViewById(R.id.address);
        personal = (TextView) findViewById(R.id.personal);
        writer = (TextView) findViewById(R.id.writer);
        prose = (TextView) findViewById(R.id.prose);
        theater = (TextView) findViewById(R.id.theater);

        TypeFaceHelper.setDescriptionTypeFace(this, address);
        TypeFaceHelper.setDescriptionTypeFace(this, personal);
        TypeFaceHelper.setDescriptionTypeFace(this, writer);
        TypeFaceHelper.setDescriptionTypeFace(this, prose);
        TypeFaceHelper.setDescriptionTypeFace(this, theater);

        addressLayout = (RelativeLayout) findViewById(R.id.addressLayout);
        personalLayout = (RelativeLayout) findViewById(R.id.personalLayout);
        writerLayout = (RelativeLayout) findViewById(R.id.writerLayout);
        proseLayout = (RelativeLayout) findViewById(R.id.proseLayout);
        theaterLayout = (RelativeLayout) findViewById(R.id.theaterLayout);

        addressLayout.setOnClickListener(categoryListener);
        personalLayout.setOnClickListener(categoryListener);
        writerLayout.setOnClickListener(categoryListener);
        proseLayout.setOnClickListener(categoryListener);
        theaterLayout.setOnClickListener(categoryListener);


//        bottomBar
        placesLayout = (RelativeLayout) findViewById(R.id.placesLayout);
        routesLayout = (RelativeLayout) findViewById(R.id.routesLayout);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        infoLayout = (RelativeLayout) findViewById(R.id.infoLayout);
        placesLayout.setOnClickListener(this);
        routesLayout.setOnClickListener(this);
        searchLayout.setOnClickListener(this);
        infoLayout.setOnClickListener(this);

        places_text = (TextView) findViewById(R.id.places_text);
        routes_text = (TextView) findViewById(R.id.routes_text);
        search_text = (TextView) findViewById(R.id.search_text);
        info_text = (TextView) findViewById(R.id.info_text);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this, search_edit);
        TypeFaceHelper.setDescriptionTypeFace(this, search_dis);

        TypeFaceHelper.setDescriptionTypeFace(this, places_text);
        TypeFaceHelper.setDescriptionTypeFace(this, routes_text);
        TypeFaceHelper.setDescriptionTypeFace(this, search_text);
        TypeFaceHelper.setDescriptionTypeFace(this, info_text);
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

        if(calculateDistance != null)
            calculateDistance.cancel(true);

        search_dis.setVisibility(View.INVISIBLE);
        list.setVisibility(View.INVISIBLE);

        if (s.equals("") || s == "" || s.length() == 0) {
            scroll.setVisibility(View.VISIBLE);
            progress.setVisibility(View.INVISIBLE);
            clear.setVisibility(View.VISIBLE);
        } else {
            scroll.setVisibility(View.INVISIBLE);
            progress.setVisibility(View.VISIBLE);
            clear.setVisibility(View.INVISIBLE);

            placesList.clear();

            List<TagsPlace> tagsList = null;

            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU))
                tagsList = TagsPlace.getByName(s.toString());
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN))
                tagsList = TagsPlace.getByNameEn(s.toString());

//            если введенная строка похожа на "музей", добавляем музей булгакова первым
            String ru = "музей";
            String en = "museum";
            if(ru.contains(s) || en.contains(s))
                placesList.add(Place.getByPlaceId(5));

//        у каждого места есть много тегов
//        согласно архитектуре базы данных, по запросу отправленному выше, ответ может содержать разные хештеги,
//        присвоенные одному месту (Например ввели "муз", найдет место,
//        у которого есть хештег: "музей", но если есть еще тег "музыка", то место повторится.
//        Для того, что бы избежать повторений в поисковом запросе,
//        добавляем в список только те объекты, ParentId которых не повторялся

            List<Integer> mainId_list = new ArrayList<>();
            if(tagsList != null && !tagsList.isEmpty()){
                search_dis.setVisibility(View.INVISIBLE);
                for(TagsPlace t : tagsList){
                    if(!mainId_list.contains(t.getTagsParentId())){
                        Place p = Place.getByPlaceId(t.getTagsParentId());
                        if(!p.getPlaceInvisible()){
                            mainId_list.add(t.getTagsParentId());
                            placesList.add(Place.getByPlaceId(t.getTagsParentId()));
                        }
                    }
                }

                calculateDistance = new CalculateDistance(this, this);
                calculateDistance.execute(placesList);

            }else{
                search_dis.setVisibility(View.VISIBLE);
                search_dis.setText(getResources().getString(R.string.search_dis));
                progress.setVisibility(View.INVISIBLE);
            }
        }
    }

    @Override
    public void afterTextChanged(Editable s) {

    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Place p = (Place) searchListAdapter.getItem(position);

        if(p != null){

            InputMethodManager imm = (InputMethodManager) this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(list.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);

            Intent intent = new Intent (this, MarkerActivity.class);
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                intent.putExtra(Place.NAME, p.getPlaceNameEn());
            }
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                intent.putExtra(Place.NAME, p.getPlaceName());
            }
            this.startActivity(intent);
            overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);

        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.placesLayout:
                intent = new Intent(SearchActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.routesLayout:
                intent = new Intent(SearchActivity.this, RoutesActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.searchLayout:
//                уже находимся на этом экране
                break;
            case R.id.infoLayout:
                intent = new Intent(SearchActivity.this, InfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            default:
                break;
        }
    }

    View.OnClickListener clearListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            search_edit.setText("");

            InputMethodManager imm = (InputMethodManager) SearchActivity.this
                    .getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(list.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        updateLocale();
    }

    private void updateLocale(){

        PreferencesHelper helper = new PreferencesHelper(this);

        if(helper.hasLocal())
            new LocaleHelper(this, helper.getLocal());

        search_edit.setHint(getResources().getString(R.string.search));
        search_dis.setHint(getResources().getString(R.string.search_dis));
        places_text.setText(getResources().getString(R.string.places_text));
        routes_text.setText(getResources().getString(R.string.routes_text));
        search_text.setText(getResources().getString(R.string.search_text));
        info_text.setText(getResources().getString(R.string.info_text));

        address.setText(getResources().getString(R.string.category_address));
        personal.setText(getResources().getString(R.string.category_personal));
        writer.setText(getResources().getString(R.string.category_writer));
        prose.setText(getResources().getString(R.string.category_prose));
        theater.setText(getResources().getString(R.string.category_theater));

        searchListAdapter.updateData(placesList);
    }

    @Override
    public void neverHadLocation() {
        if(calculateDistance != null)
            calculateDistance.cancel(true);

        searchListAdapter.updateData(placesList);
        progress.setVisibility(View.INVISIBLE);
        list.setVisibility(View.VISIBLE);

        Log.d("myLog", "neverHadLocation");
    }

    @Override
    public void distanceWasCount(List<Place> list) {

        if(calculateDistance != null)
            calculateDistance.cancel(true);

//        удаляем место "Музей Булгакова"
        if(list.contains(Place.getByPlaceId(5))){
//            удаляем место "Музей Булгакова"
            list.remove(Place.getByPlaceId(5));
//            добавляем место "Музей Булгакова"
            List<Place> places = new ArrayList<>();
            places.add(Place.getByPlaceId(5));
            places.addAll(list);

            searchListAdapter.updateData(places);
        }
        else{
            searchListAdapter.updateData(list);
        }
        progress.setVisibility(View.INVISIBLE);
        this.list.setVisibility(View.VISIBLE);

        Log.d("myLog", "distanceWasCount");
    }

    View.OnClickListener categoryListener = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            Intent cat = new Intent(SearchActivity.this, CategorySearchActivity.class);
            switch (v.getId()){
                case R.id.addressLayout:
                    cat.putExtra("category", ThemesPlace.ADDRESS_BULGAKOV);
                    break;
                case R.id.personalLayout:
                    cat.putExtra("category", ThemesPlace.PERSONAL_LIFE);
                    break;
                case R.id.writerLayout:
                    cat.putExtra("category", ThemesPlace.WRITER_AND_GOVERMENT);
                    break;
                case R.id.proseLayout:
                    cat.putExtra("category", ThemesPlace.PROSE);
                    break;
                case R.id.theaterLayout:
                    cat.putExtra("category", ThemesPlace.THEATER);
                    break;
                default:
                    break;
            }
            startActivity(cat);
            overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
        }
    };
}

