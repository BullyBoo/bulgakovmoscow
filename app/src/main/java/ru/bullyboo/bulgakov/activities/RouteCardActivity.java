package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.route.PointsRoute;
import ru.bullyboo.bulgakov.data.route.PreviewRoute;
import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * карточка маршрута
 */

public class RouteCardActivity extends Activity implements View.OnClickListener{

    private TextView activityTitle;
    private ImageView routeImage;
    private ImageView back;
    private TextView title, description;
    private TextView distance, numPlaces, time;
    private TextView createRoute;
    private RelativeLayout createRouteLayout;

    private TextView places_text, routes_text, search_text, info_text;
    private RelativeLayout placesLayout, routesLayout, searchLayout, infoLayout;

    private int routeId = 0;
    private Route route;

    public static final String MAIN_URL = "http://mobile.bulgakovmuseum.ru";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.route_card_activity);

        activityTitle = (TextView) findViewById(R.id.activity_title);
        TypeFaceHelper.setDescriptionTypeFace(this, activityTitle);

        routeImage = (ImageView) findViewById(R.id.routeImage);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
            }
        });

        title = (TextView) findViewById(R.id.title);
        description = (TextView) findViewById(R.id.description);
        TypeFaceHelper.setTitleTypeFace(this, title);
        TypeFaceHelper.setDescriptionTypeFace(this, description);

        distance = (TextView) findViewById(R.id.distance);
        numPlaces = (TextView) findViewById(R.id.numPlaces);
        time = (TextView) findViewById(R.id.time);
        TypeFaceHelper.setDescriptionTypeFace(this, distance);
        TypeFaceHelper.setDescriptionTypeFace(this, numPlaces);
        TypeFaceHelper.setDescriptionTypeFace(this, time);

        createRoute = (TextView) findViewById(R.id.create_route);
        TypeFaceHelper.setDescriptionTypeFace(this, createRoute);

        createRouteLayout = (RelativeLayout) findViewById(R.id.create_route_layout);
        createRouteLayout.setOnClickListener(createRouteListener);

//        bottomBar
        placesLayout = (RelativeLayout) findViewById(R.id.placesLayout);
        routesLayout = (RelativeLayout) findViewById(R.id.routesLayout);
        searchLayout = (RelativeLayout) findViewById(R.id.searchLayout);
        infoLayout = (RelativeLayout) findViewById(R.id.infoLayout);
        placesLayout.setOnClickListener(this);
        routesLayout.setOnClickListener(this);
        searchLayout.setOnClickListener(this);
        infoLayout.setOnClickListener(this);

        places_text = (TextView) findViewById(R.id.places_text);
        routes_text = (TextView) findViewById(R.id.routes_text);
        search_text = (TextView) findViewById(R.id.search_text);
        info_text = (TextView) findViewById(R.id.info_text);
        TypeFaceHelper.setDescriptionTypeFace(this, places_text);
        TypeFaceHelper.setDescriptionTypeFace(this, routes_text);
        TypeFaceHelper.setDescriptionTypeFace(this, search_text);
        TypeFaceHelper.setDescriptionTypeFace(this, info_text);

//        получаем id маршрута
        Intent intent = getIntent();
        if(intent != null){
            if(intent.hasExtra("route_id")){
                routeId = intent.getExtras().getInt("route_id");
            } else {
                finish();
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
            }
        } else {
            finish();
            overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
        }

//        получаем preview изображение маршрута
        if(routeId != 0){
            route = Route.getRouteById(routeId);

            PreviewRoute previewRoute = PreviewRoute.getPreviewRouteByParentId(routeId);
            if(previewRoute != null){
                Picasso.with(this)
                        .load(MAIN_URL + previewRoute.getRoutePreviewUrl())
                        .error(R.drawable.zaglushka)
                        .into(routeImage);
            } else {
                LinearLayout parent = (LinearLayout) findViewById(R.id.scroll_ll);
                RelativeLayout imageLayout = (RelativeLayout) findViewById(R.id.imageLayout);
                parent.removeView(imageLayout);
            }
        }
    }

    @Override
    public void onClick(View v) {
        Intent intent = null;

        switch (v.getId()) {
            case R.id.placesLayout:
                intent = new Intent(RouteCardActivity.this, MainActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.routesLayout:
                finish();
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
                break;
            case R.id.searchLayout:
                intent = new Intent(RouteCardActivity.this, SearchActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            case R.id.infoLayout:
                intent = new Intent(RouteCardActivity.this, InfoActivity.class);
                startActivity(intent);
                overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                break;
            default:
                break;
        }
    }

//    построение маршрута
    View.OnClickListener createRouteListener = new View.OnClickListener(){
        @Override
        public void onClick(View v) {
            PreferencesHelper helper = new PreferencesHelper(RouteCardActivity.this);
            helper.saveRouteId(routeId);
            Intent intent = new Intent(RouteCardActivity.this, MainActivity.class);
            startActivity(intent);
            overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);

        }
    };

    @Override
    protected void onResume() {
        super.onResume();
        updateLocale();
    }

    private void updateLocale(){

        PreferencesHelper helper = new PreferencesHelper(this);

        if(helper.hasLocal())
            new LocaleHelper(this, helper.getLocal());

        if(route != null){
            activityTitle.setText(getResources().getString(R.string.route));
            createRoute.setText(getResources().getString(R.string.create_route));

            places_text.setText(getResources().getString(R.string.places_text));
            routes_text.setText(getResources().getString(R.string.routes_text));
            search_text.setText(getResources().getString(R.string.search_text));
            info_text.setText(getResources().getString(R.string.info_text));

            distance.setText(route.getRouteDistance()
                    + " "
                    + getResources().getString(R.string.km));

            List<PointsRoute> pointsRoute = PointsRoute.getPointsRouteByParentId(routeId);
            if(pointsRoute != null && !pointsRoute.isEmpty()){
                numPlaces.setText(pointsRoute.size()
                        + " "
                        + getResources().getString(R.string.num_places));
            }

            float dur = Float.parseFloat(route.getRouteDuration().replace(",", "."));

            float hour = dur - (dur%1);
            float min = 60 * (dur%1);


            time.setText((int)hour
                    + " "
                    + getResources().getString(R.string.hour)
                    + " "
                    + (int)min
                    + " "
                    + getResources().getString(R.string.min));

            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                title.setText(route.getRouteName().toUpperCase());
                description.setText(route.getRouteTextMain());

            } else if (helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                title.setText(route.getRouteNameEn().toUpperCase());
                description.setText(route.getRouteTextMainEn());
            }
        } else {
            finish();
            overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
        }
    }
}
