package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.adapters.SearchListAdapter;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.ThemesPlace;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Created by BullyBoo on 19.10.2016.
 */

public class CategorySearchActivity extends Activity implements View.OnClickListener, AdapterView.OnItemClickListener{

    private TextView title;
    private ImageView back;

    private ListView list;
    private SearchListAdapter adapter;

    private int category = 0;

    private PreferencesHelper helper;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.category_search_activity);

        helper = new PreferencesHelper(this);

        title = (TextView) findViewById(R.id.activity_title);
        TypeFaceHelper.setDescriptionTypeFace(this, title);

        back = (ImageView) findViewById(R.id.back);
        back.setOnClickListener(this);

        list = (ListView) findViewById(R.id.list);
        list.setOnItemClickListener(this);

        Intent intent = getIntent();

        if(intent.hasExtra("category")){
            category = intent.getExtras().getInt("category");
        } else{
            finishActivity();
        }

        List<ThemesPlace> themes = ThemesPlace.getById(category);

//        настраиваем title
        String titleText = "";

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN))
            titleText = ThemesPlace.getById(category).get(0).getThemesNameEn();

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU))
            titleText = ThemesPlace.getById(category).get(0).getThemesName();

        title.setText(titleText);

//        получаем список мест
        List<Place> places = new ArrayList<>();

        if(!themes.isEmpty() || themes != null){
            for(ThemesPlace t : themes) {
                places.add(Place.getByPlaceId(t.getThemesParentId()));
            }

        } else {
            finishActivity();
        }

        adapter = new SearchListAdapter(this, places);

        list.setAdapter(adapter);
    }

    private void finishActivity(){
        Toast.makeText(this, getResources().getString(R.string.error), Toast.LENGTH_LONG).show();
        finish();
        overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.back){
            finish();
            overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Place p = (Place) adapter.getItem(position);

        if(p != null){
            Intent intent = new Intent (this, MarkerActivity.class);

            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN))
                intent.putExtra(Place.NAME, p.getPlaceNameEn());

            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU))
                intent.putExtra(Place.NAME, p.getPlaceName());

            startActivity(intent);
            overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
        }
    }
}
