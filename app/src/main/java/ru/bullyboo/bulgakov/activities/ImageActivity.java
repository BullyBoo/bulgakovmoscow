package ru.bullyboo.bulgakov.activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.adapters.ViewPagerAdapter;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Данный экран отображет изображения объекта Places, если они имеются.
 * На вход данный класс получает позицию ViewPagger из MarkerActivity,
 * чтобы установить на этом экране ту же позицию
 *
 * Когда пользователь закрывает этот экран, ImageActivity возвращет позицию
 * последнего изображения, чтобы в MarkerActivity установить ту же позицию
 *
 */
public class ImageActivity extends Activity implements ViewPager.OnPageChangeListener{

    private ImageView goBack;
    private TextView imageNumber;
    private ViewPager viewPager;
    private RelativeLayout mainLayout, topLayout;

    private ViewPagerAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_activity);

        imageNumber = (TextView) findViewById(R.id.imageNumber);
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        mainLayout = (RelativeLayout) findViewById(R.id.mainLayout);
        topLayout = (RelativeLayout) findViewById(R.id.topLayout);

        TypeFaceHelper.setDescriptionTypeFace(this, imageNumber);

        goBack = (ImageView) findViewById(R.id.back);
        goBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                returnLastImage();
                finish();
                overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
            }
        });
//        получаем значения полученные из MarkerActivity
        int id = getIntent().getExtras().getInt("pic_id");
        int cur_pos = getIntent().getExtras().getInt("pos_num");

//        по полученным значениям получаем данные из базы данных и выводим их в ViewPager
        Place place = Place.getByPlaceId(id);

        if(place != null){
            adapter = new ViewPagerAdapter(this, this,
                    place, false, false);
            viewPager.setAdapter(adapter);

            viewPager.setCurrentItem(cur_pos);
            viewPager.setOnPageChangeListener(this);

//        настриваем textView на установленную в пейджере позицию
            imageNumber.setText(viewPager.getCurrentItem()+1 + "/" + adapter.getCount());
        }
    }

    private void returnLastImage(){
//        отправляем позицию пейджера в MarkerActivity
        Intent intent = new Intent();
        intent.putExtra("im_num", viewPager.getCurrentItem());
        setResult(RESULT_OK, intent);
    }
    @Override
    public void onBackPressed() {
        returnLastImage();
        finish();
        overridePendingTransition(R.anim.to_right_anim_start,R.anim.to_right_anim_end);
    }

    @Override
    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
    }

    @Override
    public void onPageSelected(int position) {
//        если пользователь пролистал страницу, обновляем номер картинки
        imageNumber.setText(position+1 + "/" + adapter.getCount());
    }

    @Override
    public void onPageScrollStateChanged(int state) {

    }
}
