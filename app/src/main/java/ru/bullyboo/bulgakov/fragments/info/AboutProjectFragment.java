package ru.bullyboo.bulgakov.fragments.info;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Фрагмент "О проекте"
 */
public class AboutProjectFragment extends Fragment {

    private TextView title, text;
    private ImageView back;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.about_project_layout, null);

        title = (TextView) v.findViewById(R.id.activity_title);
        text = (TextView) v.findViewById(R.id.text);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(this.getActivity(), title);
        TypeFaceHelper.setDescriptionTypeFace(this.getActivity(), text);

        back = (ImageView) v.findViewById(R.id.back);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                вытаскиваем из стека последний фрагмент
                FragmentManager fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentManager.popBackStack();
                fragmentTransaction
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();
            }
        });

        return v;
    }
    @Override
    public void onResume() {
        super.onResume();
        updateLocale();
    }
    private void updateLocale(){

        PreferencesHelper helper = new PreferencesHelper(this.getActivity());

        if(helper.hasLocal())
            new LocaleHelper(this.getActivity(), helper.getLocal());

        title.setText(getResources().getString(R.string.about_project));
        text.setText(getResources().getString(R.string.about_project_text));
    }
}
