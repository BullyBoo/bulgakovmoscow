package ru.bullyboo.bulgakov.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.activities.MarkerActivity;
import ru.bullyboo.bulgakov.adapters.ListPlacesAdapter;
import ru.bullyboo.bulgakov.asunctasks.CalculateDistance;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.interfaces.CalculateDistanceState;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Данный фрагмент содержит список маркреров, отосортированных по удаленности
 * от последней известной приложению геолокации пользователя
 * Сортировка происходит в отдельном потоке - CalculateDistance.
 */
public class ListPlacesFragment extends Fragment implements AdapterView.OnItemClickListener
        , CalculateDistanceState{

    private ProgressBar progressBar;
    private ListView listView;
    private TextView text;

    private ListPlacesAdapter listPlacesAdapter;

    private List<Place> list = null;

    private PreferencesHelper helper;

    private CalculateDistance calculateDistance;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.list_places_fragment, null);

        helper = new PreferencesHelper(this.getActivity());

        progressBar = (ProgressBar) v.findViewById(R.id.progressBar);
        listView = (ListView) v.findViewById(R.id.listView);
        listView.setOnItemClickListener(this);

        text = (TextView) v.findViewById(R.id.text);
        text.setVisibility(View.INVISIBLE);
        TypeFaceHelper.setDescriptionTypeFace(this.getActivity(), text);

        progressBar.setVisibility(View.VISIBLE);

        return v;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Place p = list.get(position);

        if(p != null){

            Intent intent = new Intent (this.getActivity(), MarkerActivity.class);
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                intent.putExtra(Place.NAME, p.getPlaceNameEn());
            }
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                intent.putExtra(Place.NAME, p.getPlaceName());
            }
            this.startActivity(intent);
            getActivity().overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);

        }
    }

//    приложение никогда не получало геопозицию пользователя
    @Override
    public void neverHadLocation() {
//        закрываем поток, чтобы избежать утечки памяти
        Log.d("myLog", "Приложение никогда не получало координат пользователя");
        if(calculateDistance != null)
            calculateDistance.cancel(true);

        progressBar.setVisibility(View.INVISIBLE);

        text.setVisibility(View.VISIBLE);
    }

    @Override
    public void distanceWasCount(List<Place> list) {
        Log.d("myLog", "Дистанция для всех мест подсчитана");
        this.list = list;

        if(list != null){

            listPlacesAdapter = new ListPlacesAdapter(list, this.getActivity());

            listView.setAdapter(listPlacesAdapter);

            listView.setVisibility(View.VISIBLE);

            progressBar.setVisibility(View.INVISIBLE);
        }
//        закрываем поток, чтобы избежать утечки памяти
        if(calculateDistance != null)
            calculateDistance.cancel(true);
    }

    @Override
    public void onResume() {
        super.onResume();

        calculateDistance = new CalculateDistance(this.getActivity(), this);
        calculateDistance.execute(Place.getAllVisible());

        text.setVisibility(View.INVISIBLE);
        listView.setVisibility(View.INVISIBLE);
        progressBar.setVisibility(View.VISIBLE);
    }

    public void fihishListPlacesFragment() {
        super.onPause();
        FragmentManager fragmentManager = getFragmentManager();
        fragmentManager.popBackStack();
        fragmentManager.beginTransaction()
                .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                .commit();
    }

//    закрываем поток, чтобы избежать утечки памяти
    @Override
    public void onDestroy() {
        super.onDestroy();
        if(calculateDistance != null)
            calculateDistance.cancel(true);
    }
}
