package ru.bullyboo.bulgakov.fragments.info;

import android.app.AlertDialog;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.activities.InfoActivity;
import ru.bullyboo.bulgakov.tools.LocaleHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Фрагмент "Настройки"
 */
public class SettingsFragment extends Fragment implements View.OnClickListener{

    private RelativeLayout russian, english;
    private TextView title, russian_text, english_text;

    private ImageView back;

    private FragmentManager fragmentManager;

    private PreferencesHelper helper;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.settings_fragment, null);

        helper = new PreferencesHelper(this.getActivity());

        russian = (RelativeLayout) v.findViewById(R.id.russian);
        english = (RelativeLayout) v.findViewById(R.id.english);

        russian_text = (TextView) v.findViewById(R.id.russian_text);
        english_text = (TextView) v.findViewById(R.id.english_text);
        title = (TextView) v.findViewById(R.id.activity_title);

//        настройка шрифта
        TypeFaceHelper.setDescriptionTypeFace(this.getActivity(), russian_text);
        TypeFaceHelper.setDescriptionTypeFace(this.getActivity(), english_text);
        TypeFaceHelper.setDescriptionTypeFace(this.getActivity(), title);

        back = (ImageView) v.findViewById(R.id.back);

        russian.setOnClickListener(this);
        english.setOnClickListener(this);
        back.setOnClickListener(this);

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
            english_text.setTextColor(getResources().getColor(R.color.green));
        } else if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
            russian_text.setTextColor(getResources().getColor(R.color.green));
        }
        return v;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.russian:
                helper.saveLocal(PreferencesHelper.LOCAL_RU);
                updateLocale();
                russian_text.setTextColor(getResources().getColor(R.color.green));
                english_text.setTextColor(getResources().getColor(R.color.black));
                break;
            case R.id.english:
                helper.saveLocal(PreferencesHelper.LOCAL_EN);
                updateLocale();
                russian_text.setTextColor(getResources().getColor(R.color.black));
                english_text.setTextColor(getResources().getColor(R.color.green));
                break;
            case R.id.back:
                fragmentManager = getFragmentManager();
                FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

                fragmentManager.popBackStack();
                fragmentTransaction
                        .setTransition(FragmentTransaction.TRANSIT_FRAGMENT_OPEN)
                        .commit();
                break;
            default:
                break;
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        updateLocale();
    }

    private void updateLocale(){
        if(helper.hasLocal())
            new LocaleHelper(this.getActivity(), helper.getLocal());

        russian_text.setText(getResources().getString(R.string.ru_local));
        english_text.setText(getResources().getString(R.string.en_local));
        title.setText(getResources().getString(R.string.settings));

        ((InfoActivity)getActivity()).updateLocale();
    }
}
