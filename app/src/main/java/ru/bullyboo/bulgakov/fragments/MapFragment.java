package ru.bullyboo.bulgakov.fragments;

import android.app.Fragment;
import android.content.res.AssetManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.activities.MainActivity;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.interfaces.MapState;
import ru.bullyboo.bulgakov.tools.MapHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;

/**
 * Фрагмент, в котором хранится MapView - основной функционал данного приложения.
 * В данном классе реализованы кнопки:
 * ......централизация на позиции пользователя
 * ......увеличение масштаба
 * ......уменьшение масштаба
 * ......сброс маршрута
 *
 */
public class MapFragment extends Fragment implements View.OnClickListener, MapState{

    private MapView mapView;
    private MapboxMap mapBox;
    private ProgressBar progressBar;
    private ImageView myLocation;
    public MapHelper mapHelper;

    private PreferencesHelper helper;

    private Bundle extras;

    public boolean hasARoute = false;
    public int route_id = 0;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        MapboxAccountManager.start(this.getActivity(), getString(R.string.map_box_access_token));

        View v = inflater.inflate(R.layout.map_fragment, null);

        helper = new PreferencesHelper(this.getActivity());

        extras = getArguments();

//        включаем ProgressBar на время загрузки карты
        progressBar = (ProgressBar) v.findViewById(R.id.progress);
        progressBar.setVisibility(View.VISIBLE);

        myLocation = (ImageView) v.findViewById(R.id.locationButton);

        myLocation.setOnClickListener(this);

        mapView = (MapView) v.findViewById(R.id.mapview);
        mapView.onCreate(savedInstanceState);

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
//                карты загрузились, прячем ProgressBar

                MapFragment.this.progressBar.setVisibility(View.INVISIBLE);

                mapBox = mapboxMap;

                mapBox.setStyleUrl(getActivity().getResources().getString(R.string.styleUrl));


            }
        });

        return v;
    }

    public void createMap(){
        mapHelper = new MapHelper(MapFragment.this.getActivity()
                , MapFragment.this.getActivity()
                , mapView
                , mapBox
                , MapFragment.this
                , MapFragment.this);
    }
    @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();

//        если класс MapHelper не пустой и маркеры были добавлены,
//        удаляем все маркеры и наносим их заново
//        это необходимо для обновления данных о пройденных точках
        if(mapHelper!= null){
            if(mapHelper.markersWasAdd){
                mapHelper.mapClear();
                mapHelper.addMarkersOnMap(Place.getAll(), false);
            }
        }
//        если есть маршрут и класс помощник - MapHelper не пуст, строим маршрут
        if(hasARoute && mapHelper != null){
            mapHelper.createRoute(route_id);
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
        this.onDestroyView();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        mapView.onSaveInstanceState(outState);
    }

//    кнопка "моя позиция"
    @Override
    public void onClick(View v) {
        mapHelper.updateCameraPosition();
    }

    @Override
    public void mapWasDownload() {
        if(extras != null){
            Place p = Place.getByPlaceId(extras.getInt("id"));
            Location location = new Location(LocationManager.GPS_PROVIDER);

            if(p != null){
                location.setLatitude(p.getPlaceLat());
                location.setLongitude(p.getPlaceLng());

                mapHelper.updatePlaceCameraPosition(location);
            }else{
                Toast.makeText(this.getActivity(),
                        "Произошла ошибка",
                        Toast.LENGTH_LONG).show();
            }
        }
    }
}
