package ru.bullyboo.bulgakov.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Адаптер для списка отображающего дистанцию до маркера
 * Список отсотирован в порядке возрастания дистанции до маркера
 *
 */
public class ListPlacesAdapter extends BaseAdapter {

    public List<Place> list;
    private Context context;

    private PreferencesHelper helper;

    public ListPlacesAdapter(List<Place> list, Context context){
        this.list = list;
        this.context = context;

        helper = new PreferencesHelper(context);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        if(convertView == null) convertView = inflater.inflate(R.layout.list_item, parent, false);

        TextView placeName = (TextView) convertView.findViewById(R.id.name);
        TextView placeAddress = (TextView) convertView.findViewById(R.id.address);
        TextView placeDistance = (TextView) convertView.findViewById(R.id.distanse);

        TypeFaceHelper.setDescriptionTypeFace(context, placeName);
        TypeFaceHelper.setDescriptionTypeFace(context, placeAddress);
        TypeFaceHelper.setDescriptionTypeFace(context, placeDistance);

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
            placeName.setText(list.get(position).getPlaceName());
            placeAddress.setText(list.get(position).getPlaceAddress());

        } else if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
            placeName.setText(list.get(position).getPlaceNameEn());
            placeAddress.setText(list.get(position).getPlaceAddressEn());
        }

        int dist = (int)list.get(position).getPlaceDistance();

        if(dist >= 1000){
            int distance = dist/100;
            int km = (int) distance/10;
            int m = (int) distance%10;
            placeDistance.setText(km + "," + m + " " + context.getResources().getString(R.string.km));
        } else
            placeDistance.setText(dist + " " + context.getResources().getString(R.string.meters));



        return convertView;
    }
}
