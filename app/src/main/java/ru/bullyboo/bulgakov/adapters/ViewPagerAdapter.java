package ru.bullyboo.bulgakov.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v4.view.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.activities.ImageActivity;
import ru.bullyboo.bulgakov.data.place.PicturesPlace;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.PreviewPlace;

/**
 * Адаптер для отображения изображний, присвоенных маркеру согласно базе данных.
 * Данный адаптер сам обращается к базе данных и получает ссылки на изображения.
 * Загрузка изображений так же происходит в самом адаптере при помощи библиотеки
 * Picasso, которая помогает избежать ошибки OutOfMemory, а так же кеширует изображения.
 */
public class ViewPagerAdapter extends PagerAdapter {

    public static final String MAIN_URL = "http://mobile.bulgakovmuseum.ru";

    private Context context;
    private Activity activity;
    private Place place;
    private List<String> pictures;

    private boolean centerCrop;
    private boolean imageClicable;

    public ViewPagerAdapter(Context context, Activity activity, Place place, boolean centerCrop, boolean imageClicable){
        this.context = context;
        this.activity = activity;
        this.place = place;
        this.centerCrop = centerCrop;
        this.imageClicable = imageClicable;

        init();
    }

    private void init(){
        pictures = new ArrayList<>();

        PreviewPlace preview = PreviewPlace.getByParentId(place.getPlaceId());
        pictures.add(preview.getPreviewUrl());

        List<PicturesPlace> pic = PicturesPlace.getByParentId(place.getPlaceId());

        for(PicturesPlace p : pic)
            pictures.add(p.getPicturesUrl());

    }

    @Override
    public int getCount() {
        return pictures.size();
    }

    @Override
    public boolean isViewFromObject(View view, Object object) {
        return view.equals(object);
    }

    @Override
    public Object instantiateItem(ViewGroup collection, int position){

//        Pictures picture = pictures.get(position);
//
//        String picture_url = picture.getPicturesUrl();

        String picture_url = MAIN_URL +  pictures.get(position).replace("\\\\/", "/");

        ImageView image = new ImageView(context);

        if(imageClicable){
            Clicker clicker = new Clicker(position);
            image.setOnClickListener(clicker);
        }

        if(centerCrop)
            image.setScaleType(ImageView.ScaleType.CENTER_CROP);

        Picasso.with(context).load(picture_url).error(R.drawable.zaglushka).into(image);

        collection.addView(image, 0);
        return image;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object) {
        container.removeView((ImageView) object);
    }

    private class Clicker implements View.OnClickListener{

        private int position;

        public Clicker(int position){
            this.position = position;
        }

        @Override
        public void onClick(View v) {
            Intent intent = new Intent(context, ImageActivity.class);
            intent.putExtra("pic_id", place.getPlaceId());
            intent.putExtra("pos_num", position);
            activity.startActivityForResult(intent, 1);
            activity.overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
        }
    }
}
