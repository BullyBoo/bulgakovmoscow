package ru.bullyboo.bulgakov.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;

/**
 * Created by BullyBoo on 20.10.2016.
 */

public class RouteListAdapter extends BaseAdapter {

    private Context context;
    private List<Route> routes;

    private PreferencesHelper helper;

    public RouteListAdapter(Context context, List<Route> routes){
        this.context = context;
        this.routes = routes;

        helper = new PreferencesHelper(context);
    }

    @Override
    public int getCount() {
        return routes.size();
    }

    @Override
    public Object getItem(int position) {
        return routes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        if(convertView == null) convertView = inflater.inflate(R.layout.route_list_item, parent, false);

        TextView routeName = (TextView) convertView.findViewById(R.id.name);

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
            routeName.setText(routes.get(position).getRouteName());
        } else if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
            routeName.setText(routes.get(position).getRouteNameEn());
        }
        return convertView;
    }
}
