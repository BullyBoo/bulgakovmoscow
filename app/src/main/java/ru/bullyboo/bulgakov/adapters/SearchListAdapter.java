package ru.bullyboo.bulgakov.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Адаптер для списка, выводящего объекты Places подходящие по
 * поисковому запросу
 */
public class SearchListAdapter extends BaseAdapter {

    private List<Place> list;
    private Context context;
    private PreferencesHelper helper;
    private TypeFaceHelper typeFaceHelper;

    public SearchListAdapter(Context context, List<Place> list){
        this.list = list;
        this.context = context;

        helper = new PreferencesHelper(context);
    }

    public void updateData(List<Place> list){
        this.list = list;
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return list.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = LayoutInflater.from(context);

        if(convertView == null) convertView = inflater.inflate(R.layout.search_list_item, parent, false);

        TextView placeName = (TextView) convertView.findViewById(R.id.name);
        TextView placeAddress = (TextView) convertView.findViewById(R.id.address);

//        настраиваем шрифт
        TypeFaceHelper.setDescriptionTypeFace(context, placeName);
        TypeFaceHelper.setDescriptionTypeFace(context, placeAddress);

//        Log.d("myLog", "position = " + position + " distance = " + list.get(position).getPlaceDistance());

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)) {
            placeName.setText(list.get(position).getPlaceName());
            placeAddress.setText(list.get(position).getPlaceAddress());
        } else if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)) {
            placeName.setText(list.get(position).getPlaceNameEn());
            placeAddress.setText(list.get(position).getPlaceAddressEn());
        }

        return convertView;
    }
}
