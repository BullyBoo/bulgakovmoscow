package ru.bullyboo.bulgakov.asunctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.ActiveAndroid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import ru.bullyboo.bulgakov.data.route.PointsLentaRoute;
import ru.bullyboo.bulgakov.data.route.PointsRoute;
import ru.bullyboo.bulgakov.data.route.PolylineRoute;
import ru.bullyboo.bulgakov.data.route.PreviewRoute;
import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.data.route.RouteJson;
import ru.bullyboo.bulgakov.data.route.TagsRoute;
import ru.bullyboo.bulgakov.interfaces.DownloadDataState;
import ru.bullyboo.bulgakov.tools.CheckInternetConnection;


/**
 * Класс-загрузчик маршрутов
 * В нем происходит проверка версии Json, если на данный момент в приложении стоит та же версия,
 * Json обрабатываться не будет
 * В противном случае, удаляется база данных и строится заново
 *
 * Данный класс:
 * ......загружает данные
 * ......обрабатывает данные
 * ......парсит Json
 * ......записывает данные в базу данных
 *
 */
public class RouteLoader extends AsyncTask<Void, Void, String> {

    private HttpURLConnection urlConnection;
    private BufferedReader readString;
    private String resultJsonParse = "";

    private DownloadDataState status;
    private Context context;

    private int task_id = 0;

    public RouteLoader(Context context, DownloadDataState status, int task_id){
        Log.d("RouteLoader", "Создан поток загрузки маршрутов");
        this.status = status;
        this.context = context;
        this.task_id = task_id;
    }

    @Override
    protected String doInBackground(Void[] params) {
        Log.d("RouteLoader", "Поток RouteLoader начал свою работу");
//        проверяем наличие интернет соединения
        CheckInternetConnection checkInternetConnection = new CheckInternetConnection(context);

        Log.d("RouteLoader", "Подключение к интернету " + checkInternetConnection.isOnline());

        if(checkInternetConnection.isOnline()){
            try {
//                получаем данные с ресурса
                URL yandexUrl = new URL
                        ("http://www.mobile.bulgakovmuseum.ru/api/list/routes");

                urlConnection = (HttpURLConnection) yandexUrl.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream input = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                readString = new BufferedReader(new InputStreamReader(input));

                String s;

                while ((s = readString.readLine()) != null) {
                    buffer.append(s);
                }
                resultJsonParse = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
//            сообщаем о том, что соединение с интернетом отсутствует
            status.noInterentConnection(task_id);
            Log.d("RouteLoader", "Нет соединения с интернетом");
            return null;
        }
        Log.d("RouteLoader", "Конец методоа doInBackground");
        return resultJsonParse;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            JSONObject json = new JSONObject(s);

//            проверяем версию Json

            if(json != null){
                String version = "";
                int arraySize = 0;
                long lastUpdate = 0;

                if(json.has("version") && !json.isNull("version"))
                    version = json.getString("version");
                if(json.has("routes") && !json.isNull("routes")) {
                    JSONArray places = json.getJSONArray("routes");
                    arraySize = places.length();
                }
                if(json.has("lastUpdate") && !json.isNull("lastUpdate"))
                    lastUpdate = json.getLong("lastUpdate");

                RouteJson routeJsonInfo = RouteJson.getRouteJsonInfo();

                if(routeJsonInfo != null){
                    if(routeJsonInfo.getPlaceJsonVersion().equals(version) &&
                            routeJsonInfo.getPlaceJsonPlaceArraySize() == arraySize &&
                            routeJsonInfo.getPlaceJsonLastUpdate() == lastUpdate){
                        Log.d("RouteLoader", "Версия json не изменилась");
                        status.jsonVersionWasNotChange(task_id);
                        return;
                    } else {
                        clearDataBase();
                    }
                }
//                запись в таблицы
                parseJson(json);
            }

        } catch (JSONException e) {
//            сообщаем об ошибке и передаем ее
            Log.d("RouteLoader", "Неизвестная ошибка");
            status.errorDownload(e, task_id);
            e.printStackTrace();
        }
        Log.d("RouteLoader", "Поток завершил свою работу");
//        сообщаем о том, что поток завершил свою работу
        status.successfullDownload(task_id);

//        закрываем поток
        this.cancel(true);
    }

    private void clearDataBase(){
//        очищаем таблицы
        List<PointsLentaRoute> pointsLenta = PointsLentaRoute.getAll();
        for(PointsLentaRoute p : pointsLenta)
            p.delete();

        List<PointsRoute> points = PointsRoute.getAll();
        for(PointsRoute p : points)
            p.delete();

        List<PolylineRoute> polyline = PolylineRoute.getAll();
        for(PolylineRoute p : polyline)
            p.delete();

        List<PreviewRoute> preview = PreviewRoute.getAll();
        for(PreviewRoute t : preview)
            t.delete();

        List<Route> route = Route.getAll();
        for(Route t : route)
            t.delete();

        List<TagsRoute> tag = TagsRoute.getAll();
        for(TagsRoute t : tag)
            t.delete();

        RouteJson json = RouteJson.getRouteJsonInfo();
        json.delete();


//        сообщаем о том, что версия Json изменилась
        Log.d("PlaceLoader", "Версия json изменилась");
        status.jsonVersionWasChange(task_id);
    }

    private void parseJson(JSONObject json){

//        открываем транзакцию
        ActiveAndroid.beginTransaction();

        try {
//            сохраняем информацию о Json
            RouteJson routeJsonInfo = new RouteJson();

            JSONArray routeArray = json.getJSONArray("routes");

            if(json.has("version"))
                routeJsonInfo.setPlaceJsonVersion(json.getString("version"));
            if(json.has("routes")) {
                routeJsonInfo.setPlaceJsonPlaceArraySize(routeArray.length());
            }
            if(json.has("lastUpdate"))
                routeJsonInfo.setPlaceJsonLastUpdate(json.getLong("lastUpdate"));

            routeJsonInfo.save();

            for(int i = 0; i < routeArray.length(); i ++){

                Route route = new Route();

                JSONObject jsonRoute = routeArray.getJSONObject(i);
//                записываем places
                if(jsonRoute.has("id") && !jsonRoute.isNull("id"))
                    route.setRouteId(jsonRoute.getInt("id"));
                if(jsonRoute.has("updated_at") && !jsonRoute.isNull("updated_at"))
                    route.setRouteUpdateAt(jsonRoute.getLong("updated_at"));
                if(jsonRoute.has("name") && !jsonRoute.isNull("name"))
                    route.setRouteName(jsonRoute.getString("name"));
                if(jsonRoute.has("name_en") && !jsonRoute.isNull("name_en"))
                    route.setRouteNameEn(jsonRoute.getString("name_en"));
                if(jsonRoute.has("blank_address") && !jsonRoute.isNull("blank_address"))
                    route.setRouteBlankAddress(jsonRoute.getInt("blank_address"));
                if(jsonRoute.has("distance") && !jsonRoute.isNull("distance"))
                    route.setRouteDistance(jsonRoute.getString("distance"));
                if(jsonRoute.has("duration") && !jsonRoute.isNull("duration"))
                    route.setRouteDuration(jsonRoute.getString("duration"));
                if(jsonRoute.has("description") && !jsonRoute.isNull("description"))
                    route.setRouteDescription(jsonRoute.getString("description"));
                if(jsonRoute.has("text_main") && !jsonRoute.isNull("text_main"))
                    route.setRouteTextMain(jsonRoute.getString("text_main"));
                if(jsonRoute.has("description_en") && !jsonRoute.isNull("description_en"))
                    route.setRouteDescriptionEn(jsonRoute.getString("description_en"));
                if(jsonRoute.has("text_main_en") && !jsonRoute.isNull("text_main_en"))
                    route.setRouteTextMainEn(jsonRoute.getString("text_main_en"));
                if(jsonRoute.has("themes") && !jsonRoute.isNull("themes"))
                    route.setRouteTheme(jsonRoute.getInt("themes"));
//                записываем preview
                if(jsonRoute.has("preview") && !jsonRoute.isNull("preview")) {

                    PreviewRoute previewRoute = new PreviewRoute();

                    JSONObject json_preview = jsonRoute.getJSONObject("preview");

//                    устанавиливаем main_id равный place_id для поиска между двух таблиц
                    if(jsonRoute.has("id") && !jsonRoute.isNull("id")){
                        previewRoute.setRoutePreviewParentId(jsonRoute.getInt("id"));
                    }

                    if(json_preview.has("id")) previewRoute.setRoutePreviewId(json_preview.getInt("id"));
                    if(json_preview.has("url")) previewRoute.setRoutePreviewUrl(json_preview.getString("url"));

                    previewRoute.save();
                }

//                записываем tags
                if(jsonRoute.has("tags") && !jsonRoute.isNull("tags")) {
                    JSONArray json_tags_array = jsonRoute.getJSONArray("tags");

                    for(int k = 0; k < json_tags_array.length(); k++){

                        TagsRoute tagsRoute = new TagsRoute();

                        JSONObject json_tags = json_tags_array.getJSONObject(k);

//                        устанавиливаем main_id равный place_id для поиска между двух таблиц
                        if(jsonRoute.has("id") && !jsonRoute.isNull("id")){
                            tagsRoute.setRouteTagsParentId(jsonRoute.getInt("id"));
                        }

                        if(json_tags.has("id")) tagsRoute.setRouteTagsId(json_tags.getInt("id"));
                        if(json_tags.has("name")) tagsRoute.setRouteTagsName(json_tags.getString("name"));
                        if(json_tags.has("name_en")) tagsRoute.setRouteTagsNameEn(json_tags.getString("name_en"));
                        if(json_tags.has("updated_at")) tagsRoute.setRouteTagsUpdatedAt(json_tags.getLong("updated_at"));
                        if(json_tags.has("created_at")) tagsRoute.setRouteTagsCreatedAt(json_tags.getLong("created_at"));

                        tagsRoute.save();
                    }
                }

//                записываем polyline
                if(jsonRoute.has("polyline") && !jsonRoute.isNull("polyline")) {
                    JSONArray jsonPolylineArray = jsonRoute.getJSONArray("polyline");

                    for(int k = 0; k < jsonPolylineArray.length(); k++){

                        PolylineRoute polylineRoute = new PolylineRoute();

                        JSONArray jsonPolyline = jsonPolylineArray.getJSONArray(k);

                        double lat = (double) jsonPolyline.get(0);
                        double lng = (double) jsonPolyline.get(1);

                        if(jsonRoute.has("id") && !jsonRoute.isNull("id"))
                            polylineRoute.setRoutePolylineParentId(jsonRoute.getInt("id"));

                        polylineRoute.setRoutePolylineLat(lat);
                        polylineRoute.setRoutePolylineLng(lng);

                        polylineRoute.save();
                    }
                }

//                записываем points
                if(jsonRoute.has("points") && !jsonRoute.isNull("points")) {
                    JSONArray jsonPointsArray = jsonRoute.getJSONArray("points");

                    for(int k = 0; k < jsonPointsArray.length(); k++){

                        PointsRoute pointsRoute = new PointsRoute();

                        JSONObject jsonPoint = jsonPointsArray.getJSONObject(k);

                        if(jsonRoute.has("id") && !jsonRoute.isNull("id"))
                            pointsRoute.setPointsParentid(jsonRoute.getInt("id"));

                        pointsRoute.setPointsId(jsonPoint.getInt("id"));

                        pointsRoute.save();
                    }
                }

//                записываем pointsLenta
                if(jsonRoute.has("points_lenta") && !jsonRoute.isNull("points_lenta")) {
                    JSONArray jsonPointsArray = jsonRoute.getJSONArray("points_lenta");

                    for(int k = 0; k < jsonPointsArray.length(); k++){

                        PointsLentaRoute pointsLentaRoute = new PointsLentaRoute();

                        JSONObject jsonPointLenta = jsonPointsArray.getJSONObject(k);

                        if(jsonRoute.has("id") && !jsonRoute.isNull("id"))
                            pointsLentaRoute.setPointsLentaParentid(jsonRoute.getInt("id"));

                        pointsLentaRoute.setPointsLentaId(jsonPointLenta.getInt("id"));

                        pointsLentaRoute.save();
                    }
                }

                route.save();
            }
//            сообщаем о том, что транзация прошла успешно
            ActiveAndroid.setTransactionSuccessful();

        } catch (JSONException | NullPointerException e) {
//            сообщаем об ошибке и передаем ее
            Log.d("RouteLoader", "Неизвестная ошибка");
            status.errorDownload(e, task_id);
            e.printStackTrace();
        }
//        завершаем транзакцию
        ActiveAndroid.endTransaction();
    }
}
