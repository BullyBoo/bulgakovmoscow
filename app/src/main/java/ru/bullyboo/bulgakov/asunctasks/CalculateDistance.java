package ru.bullyboo.bulgakov.asunctasks;

import android.content.Context;
import android.location.Location;
import android.os.AsyncTask;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.interfaces.CalculateDistanceState;
import ru.bullyboo.bulgakov.tools.DistanceHelper;
import ru.bullyboo.bulgakov.tools.PreferencesHelper;

/**
 * В этом потоке высчитывается расстояние от текущей позиции пользователя
 * до каждого маркера, отмеченного на карте, после чего сортируются в порядке
 * возрастания (по удаленности)
 *
 * Данное действие вынесено в отдельный поток, так как занимает большое количество
 * памяти. Для более качественной работы приложения, основной поток не должен перегружаться.
 *
 */
public class CalculateDistance extends AsyncTask<List<Place>, Void, Void> {

    private PreferencesHelper helper;

    private Location myActualLocation;

    private List<Place> list = null;

    private CalculateDistanceState state;

    public CalculateDistance(Context context, CalculateDistanceState state){
        this.state = state;

        helper = new PreferencesHelper(context);
    }

    @Override
    protected Void doInBackground(List<Place>... params) {
        Log.d("myLog", "Поток начал свою работу");
        getLastPosition();
        list = countDistance(params[0]);
        Log.d("myLog", "Поток завершил свою работу");
        return null;
    }

    @Override
    protected void onPostExecute(Void s) {
        super.onPostExecute(s);
        state.distanceWasCount(list);
    }

    private void getLastPosition(){

//        получаем координаты из PreferencesHelper
        if(helper.hasLocation()){
            myActualLocation = helper.getLocation();
        }else{
            state.neverHadLocation();
            this.cancel(true);
            return;
        }

        Log.d("myLog", "myActualLocation = " + myActualLocation);

        return;

    }

    private List<Place> countDistance(List<Place> places){

        Log.d("myLog", "placesList = " + places.size());

//        создаем массив дистанций и массив индексов
        double[] distance = new double[places.size()];
        int[] index = new int[places.size()];

        int ind = 0;

//        считаем дистанцию и заносим ее в массив distance
//        а так же записываем id места, дистанция для которого была подсчитана
        for(Place p : places){

            double dist = DistanceHelper.distance(myActualLocation.getLatitude(),
                    p.getPlaceLat(),
                    myActualLocation.getLongitude(),
                    p.getPlaceLng());

            distance[ind] = dist;
            index[ind] = p.getPlaceId();

//            сохраняем полученную дистанцию
            p.setPlaceDistance(distance[ind]);
            p.save();

            ind++;
        }
        return sortirovka(distance, index);
    }

    private List<Place> sortirovka(double[] distance, int[] index){

//        сортируем массивы методом пузырька по возрастанию
        for(int i = distance.length-1; i >= 0 ; i--){
            for(int j = 0 ; j < i ; j++){

                if( distance[j] > distance[j+1] ){
                    double tmp = distance[j];
                    distance[j] = distance[j+1];
                    distance[j+1] = tmp;

                    int in = index[j];
                    index[j] = index[j+1];
                    index[j+1] = in;
                }
            }
        }

        return createNewListPlaces(index);
    }

    private List<Place> createNewListPlaces(int[] index){

        List<Place> newListPlaces = new ArrayList<>();

//        заполняем список заново в том порядке, в каком стоят индексы
        for(int i = 0; i < index.length-1; i++){
            Place p = Place.getByPlaceId(index[i]);

            newListPlaces.add(p);
        }
        return newListPlaces;
    }
}
