package ru.bullyboo.bulgakov.asunctasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.activeandroid.ActiveAndroid;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.List;

import ru.bullyboo.bulgakov.data.place.PicturesPlace;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.PlaceJson;
import ru.bullyboo.bulgakov.data.place.PreviewPlace;
import ru.bullyboo.bulgakov.data.place.TagsPlace;
import ru.bullyboo.bulgakov.data.place.ThemesPlace;
import ru.bullyboo.bulgakov.data.route.RouteJson;
import ru.bullyboo.bulgakov.interfaces.DownloadDataState;
import ru.bullyboo.bulgakov.tools.CheckInternetConnection;


/**
 * Класс-загрузчик мест
 * В нем происходит проверка версии Json, если на данный момент в приложении стоит та же версия,
 * Json обрабатываться не будет
 * В противном случае, удаляется база данных и строится заново
 *
 * Данный класс:
 * ......загружает данные
 * ......обрабатывает данные
 * ......парсит Json
 * ......записывает данные в базу данных
 *
 */
public class PlaceLoader extends AsyncTask<Void, Void, String> {

    private HttpURLConnection urlConnection;
    private BufferedReader readString;
    private String resultJsonParse = "";

    private DownloadDataState status;
    private Context context;

    private int task_id = 0;

    public PlaceLoader(Context context, DownloadDataState status, int task_id){
        Log.d("PlaceLoader", "Создан поток загрузки мест");
        this.status = status;
        this.context = context;
        this.task_id = task_id;
    }

    @Override
    protected String doInBackground(Void[] params) {
        Log.d("PlaceLoader", "Поток PlaceLoader начал свою работу");
//        проверяем наличие интернет соединения
        CheckInternetConnection checkInternetConnection = new CheckInternetConnection(context);

        Log.d("PlaceLoader", "Подключение к интернету " + checkInternetConnection.isOnline());

        if(checkInternetConnection.isOnline()){
            try {
//                получаем данные с ресурса
                URL yandexUrl = new URL
                        ("http://www.mobile.bulgakovmuseum.ru/api/list/places");

                urlConnection = (HttpURLConnection) yandexUrl.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream input = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                readString = new BufferedReader(new InputStreamReader(input));

                String s;

                while ((s = readString.readLine()) != null) {
                    buffer.append(s);
                }
                resultJsonParse = buffer.toString();

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }else{
//            сообщаем о том, что соединение с интернетом отсутствует
            status.noInterentConnection(task_id);
            Log.d("PlaceLoader", "Нет соединения с интернетом");
            return null;
        }
        Log.d("PlaceLoader", "Конец методоа doInBackground");
        return resultJsonParse;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);
        try {
            JSONObject json = new JSONObject(s);

//            проверяем версию Json

            if(json != null){
                String version = "";
                int arraySize = 0;
                long lastUpdate = 0;

                if(json.has("version") && !json.isNull("version"))
                    version = json.getString("version");
                if(json.has("places") && !json.isNull("places")) {
                    JSONArray places = json.getJSONArray("places");
                    arraySize = places.length();
                }
                if(json.has("lastUpdate") && !json.isNull("lastUpdate"))
                    lastUpdate = json.getLong("lastUpdate");

                PlaceJson placeJsonInfo = PlaceJson.getPlaceJsonInfo();

                if(placeJsonInfo != null){
                    if(placeJsonInfo.getPlaceJsonVersion().equals(version) &&
                            placeJsonInfo.getPlaceJsonPlaceArraySize() == arraySize &&
                            placeJsonInfo.getPlaceJsonLastUpdate() == lastUpdate){
                        Log.d("PlaceLoader", "Версия json не изменилась");
                        status.jsonVersionWasNotChange(task_id);
                        return;
                    } else {
                        clearDataBase();
                    }
                }
//                запись в таблицы
                parseJson(json);
            }

        } catch (JSONException e) {
//            сообщаем об ошибке и передаем ее
            Log.d("PlaceLoader", "Неизвестная ошибка");
            status.errorDownload(e, task_id);
            e.printStackTrace();
        }
        Log.d("PlaceLoader", "Поток завершил свою работу");
//        сообщаем о том, что поток завершил свою работу
        status.successfullDownload(task_id);

//        закрываем поток
        this.cancel(true);
    }

    private void clearDataBase(){
//        очищаем таблицы
        List<PicturesPlace> picture = PicturesPlace.getAll();
        for(PicturesPlace p : picture)
            p.delete();

        List<Place> place = Place.getAll();
        for(Place p : place)
            p.delete();

        List<PreviewPlace> preview = PreviewPlace.getAll();
        for(PreviewPlace p : preview)
            p.delete();

        List<TagsPlace> tag = TagsPlace.getAll();
        for(TagsPlace t : tag)
            t.delete();

        List<ThemesPlace> theme = ThemesPlace.getAll();
        for(ThemesPlace t : theme)
            t.delete();

        PlaceJson json = PlaceJson.getPlaceJsonInfo();
        json.delete();

//        сообщаем о том, что версия Json изменилась
        Log.d("PlaceLoader", "Версия json изменилась");
        status.jsonVersionWasChange(task_id);
    }

    private void parseJson(JSONObject json){

//        открываем транзакцию
        ActiveAndroid.beginTransaction();

        try {

//            сохраняем информацию о Json
            PlaceJson placeJsonInfo = new PlaceJson();

            JSONArray placesArray = json.getJSONArray("places");

            if(json.has("version"))
                placeJsonInfo.setPlaceJsonVersion(json.getString("version"));
            if(json.has("places")) {
                placeJsonInfo.setPlaceJsonPlaceArraySize(placesArray.length());
            }
            if(json.has("lastUpdate"))
                placeJsonInfo.setPlaceJsonLastUpdate(json.getLong("lastUpdate"));

            placeJsonInfo.save();

            for(int i = 0; i < placesArray.length(); i ++){

                Place place = new Place();

                JSONObject jsonPlace = placesArray.getJSONObject(i);

//                записываем places
                if(jsonPlace.has("id") && !jsonPlace.isNull("id"))
                    place.setPlaceId(jsonPlace.getInt("id"));
                if(jsonPlace.has("name") && !jsonPlace.isNull("name"))
                    place.setPlaceName(jsonPlace.getString("name"));
                if(jsonPlace.has("address") && !jsonPlace.isNull("address"))
                    place.setPlaceAddress(jsonPlace.getString("address"));
                if(jsonPlace.has("name_en") && !jsonPlace.isNull("name_en"))
                    place.setPlaceNameEn(jsonPlace.getString("name_en"));
                if(jsonPlace.has("address_en") && !jsonPlace.isNull("address_en"))
                    place.setPlaceAddressEn(jsonPlace.getString("address_en"));
                if(jsonPlace.has("blank_address") && !jsonPlace.isNull("blank_address"))
                    place.setPlaceBlankAddress(jsonPlace.getInt("blank_address"));
                if(jsonPlace.has("lat") && !jsonPlace.isNull("lat"))
                    place.setPlaceLat(jsonPlace.getDouble("lat"));
                if(jsonPlace.has("lng") && !jsonPlace.isNull("lng"))
                    place.setPlaceLng(jsonPlace.getDouble("lng"));
                if(jsonPlace.has("description") && !jsonPlace.isNull("description"))
                    place.setPlaceDescription(jsonPlace.getString("description"));
                if(jsonPlace.has("text_link") && !jsonPlace.isNull("text_link"))
                    place.setPlaceTextLink(jsonPlace.getString("text_link"));
                if(jsonPlace.has("text_main") && !jsonPlace.isNull("text_main"))
                    place.setPlaceTextMain(jsonPlace.getString("text_main"));
                if(jsonPlace.has("description_en") && !jsonPlace.isNull("description_en"))
                    place.setPlaceDescriptionEn(jsonPlace.getString("description_en"));
                if(jsonPlace.has("text_link_en") && !jsonPlace.isNull("text_link_en"))
                    place.setPlaceTextLinkEn(jsonPlace.getString("text_link_en"));
                if(jsonPlace.has("text_main_en") && !jsonPlace.isNull("text_main_en"))
                    place.setPlaceTextMainEn(jsonPlace.getString("text_main_en"));
                if(jsonPlace.has("metro") && !jsonPlace.isNull("metro"))
                    place.setPlaceMetro(jsonPlace.getString("metro"));
                if(jsonPlace.has("metro_en") && !jsonPlace.isNull("metro_en"))
                    place.setPlaceMetroEn(jsonPlace.getString("metro_en"));
                if(jsonPlace.has("invisible") && !jsonPlace.isNull("invisible"))
                    place.setPlaceInvisible(jsonPlace.getBoolean("invisible"));
                if(jsonPlace.has("created_at") && !jsonPlace.isNull("created_at"))
                    place.setPlaceCreateId(jsonPlace.getLong("created_at"));
                if(jsonPlace.has("updated_at") && !jsonPlace.isNull("updated_at"))
                    place.setPlaceUpdateAt(jsonPlace.getLong("updated_at"));

//                записываем preview
                if(jsonPlace.has("preview") && !jsonPlace.isNull("preview")) {

                    PreviewPlace previewPlace = new PreviewPlace();

                    JSONObject json_preview = jsonPlace.getJSONObject("preview");

//                    устанавиливаем main_id равный place_id для поиска между двух таблиц
                    if(jsonPlace.has("id") && !jsonPlace.isNull("id")){
                        previewPlace.setPreviewParentid(jsonPlace.getInt("id"));
                    }

                    if(json_preview.has("id")) previewPlace.setPreviewId(json_preview.getInt("id"));
                    if(json_preview.has("url")) previewPlace.setPreviewUrl(json_preview.getString("url"));

                    previewPlace.save();
                }

//                записываем pictures
                if(jsonPlace.has("pictures") && !jsonPlace.isNull("pictures")){
                    JSONArray json_picture_array = jsonPlace.getJSONArray("pictures");

                    for(int k = 0; k <json_picture_array.length(); k++){

                        PicturesPlace picturesPlace = new PicturesPlace();

                        JSONObject json_picture = json_picture_array.getJSONObject(k);

//                        устанавиливаем main_id равный place_id для поиска между двух таблиц
                        if(jsonPlace.has("id") && !jsonPlace.isNull("id")){
                            picturesPlace.setPicturesParentId(jsonPlace.getInt("id"));
                        }

                        if(json_picture.has("id")) picturesPlace.setPicturesId(json_picture.getInt("id"));
                        if(json_picture.has("url")) picturesPlace.setPicturesUrl(json_picture.getString("url"));

                        picturesPlace.save();
                    }
                }


//                записываем tags
                if(jsonPlace.has("tags") && !jsonPlace.isNull("tags")) {
                    JSONArray json_tags_array = jsonPlace.getJSONArray("tags");

                    for(int k = 0; k < json_tags_array.length(); k++){

                        TagsPlace tagsPlace = new TagsPlace();

                        JSONObject json_tags = json_tags_array.getJSONObject(k);

//                        устанавиливаем main_id равный place_id для поиска между двух таблиц
                        if(jsonPlace.has("id") && !jsonPlace.isNull("id")){
                            tagsPlace.setTagsParentId(jsonPlace.getInt("id"));
                        }

                        if(json_tags.has("id")) tagsPlace.setTagsId(json_tags.getInt("id"));
                        if(json_tags.has("name")) tagsPlace.setTagsName(json_tags.getString("name"));
                        if(json_tags.has("name_en")) tagsPlace.setTagsNameEn(json_tags.getString("name_en"));
                        if(json_tags.has("updated_at")) tagsPlace.setTagsUpdatedAt(json_tags.getLong("updated_at"));
                        if(json_tags.has("created_at")) tagsPlace.setTagsCreatedAt(json_tags.getLong("created_at"));

                        tagsPlace.save();
                    }
                }

//                записываем themes
                if(jsonPlace.has("themes") && !jsonPlace.isNull("themes")) {
                    JSONArray json_themes_array = jsonPlace.getJSONArray("themes");

                    for(int k = 0; k < json_themes_array.length(); k++){

                        ThemesPlace themesPlace = new ThemesPlace();

                        JSONObject json_themes = json_themes_array.getJSONObject(k);

//                        устанавиливаем main_id равный place_id для поиска между двух таблиц
                        if(jsonPlace.has("id") && !jsonPlace.isNull("id")){
                            themesPlace.setThemesParentId(jsonPlace.getInt("id"));
                        }

                        if(json_themes.has("id")) themesPlace.setThemesId(json_themes.getInt("id"));
                        if(json_themes.has("name")) themesPlace.setThemesName(json_themes.getString("name"));
                        if(json_themes.has("name_en")) themesPlace.setThemesNameEn(json_themes.getString("name_en"));
                        if(json_themes.has("updated_at")) themesPlace.setThemesUpdateAt(json_themes.getLong("updated_at"));
                        if(json_themes.has("created_at")) themesPlace.setThemesCreatedAt(json_themes.getLong("created_at"));

                        themesPlace.save();
                    }
                }
                place.save();
            }
//            сообщаем о том, что транзация прошла успешно
            ActiveAndroid.setTransactionSuccessful();

        } catch (JSONException | NullPointerException e) {
//            сообщаем об ошибке и передаем ее
            Log.d("PlaceLoader", "Неизвестная ошибка");
            status.errorDownload(e, task_id);
            e.printStackTrace();
        }
//        завершаем транзакцию
        ActiveAndroid.endTransaction();
    }
}
