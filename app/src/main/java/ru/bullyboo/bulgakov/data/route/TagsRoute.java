package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BullyBoo on 20.10.2016.
 */
@Table(name = "route_tags_table")
public class TagsRoute extends Model implements Serializable {

    public static final String ROUTE_TAGS_PARENT_ID = "ROUTE_TAGS_PARENT_ID";
    public static final String ROUTE_TAGS_ID = "ROUTE_TAGS_ID";
    public static final String ROUTE_TAGS_NAME = "ROUTE_TAGS_NAME";
    public static final String ROUTE_TAGS_NAME_EN = "ROUTE_TAGS_NAME_EN";
    public static final String ROUTE_TAGS_UPDATED_AT = "ROUTE_TAGS_UPDATED_AT";
    public static final String ROUTE_TAGS_CREATED_AT = "ROUTE_TAGS_CREATED_AT";

    @Column(name = ROUTE_TAGS_PARENT_ID)
    private int routeTagsParentId;

    @Column(name = ROUTE_TAGS_ID)
    private int routeTagsId;

    @Column(name = ROUTE_TAGS_NAME)
    private String routeTagsName;

    @Column(name = ROUTE_TAGS_NAME_EN)
    private String routeTagsNameEn;

    @Column(name = ROUTE_TAGS_UPDATED_AT)
    private long routeTagsUpdatedAt;

    @Column(name = ROUTE_TAGS_CREATED_AT)
    private long routeTagsCreatedAt;


    public TagsRoute(){
        super();
    }


    public void setRouteTagsParentId(int id){
        this.routeTagsParentId = id;
    }

    public void setRouteTagsId(int id){
        this.routeTagsId = id;
    }

    public void setRouteTagsName(String name){
        this.routeTagsName = name;
    }

    public void setRouteTagsNameEn(String nameEn){
        this.routeTagsNameEn = nameEn;
    }

    public void setRouteTagsUpdatedAt(long updatedAt){
        this.routeTagsUpdatedAt = updatedAt;
    }

    public void setRouteTagsCreatedAt(long createdAt){
        this.routeTagsCreatedAt = createdAt;
    }


    public int getRouteTagsParentId(){
        return this.routeTagsParentId;
    }

    public int getRouteTagsId(){
        return this.routeTagsId;
    }

    public String getRouteTagsName(){
        return this.routeTagsName;
    }

    public String getRouteTagsNameEn(){
        return this.routeTagsNameEn;
    }

    public long getRouteTagsUpdatedAt(){
        return this.routeTagsUpdatedAt;
    }

    public long getRouteTagsCreatedAt(){
        return this.routeTagsCreatedAt;
    }


    public static List<TagsRoute> getAll(){
        return new Select().from(TagsRoute.class).execute();
    }

    public static List<TagsRoute> getByParentId(int mainId){
        return new Select().from(TagsRoute.class).where(ROUTE_TAGS_PARENT_ID + " = ?", mainId).execute();
    }

    public static List<TagsRoute> getByName(String name){
        return new Select().from(TagsRoute.class).where(ROUTE_TAGS_NAME + " LIKE ?", "%" + name + "%").execute();
    }
    public static List<TagsRoute> getByNameEn(String nameEn){
        return new Select().from(TagsRoute.class).where(ROUTE_TAGS_NAME_EN + " LIKE ?", "%" + nameEn + "%").execute();
    }


}
