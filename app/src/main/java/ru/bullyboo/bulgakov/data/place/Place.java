package ru.bullyboo.bulgakov.data.place;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * База данных, в которой хранится полученная информация из Json
 */
@Table(name = "places_table")
public class Place extends Model implements Serializable {

//    перечисление констант, которые будут именами колонок
    public static final String PLACE_ID = "PLACE_ID";
    public static final String NAME = "NAME";
    public static final String ADDRESS = "ADDRESS";
    public static final String NAME_EN = "NAME_EN";
    public static final String ADDRESS_EN = "ADDRESS_EN";
    public static final String BLANK_ADDRESS = "BLANK_ADDRESS";
    public static final String LAT = "LAT";
    public static final String LNG = "LNG";
    public static final String DISTANCE = "DISTANCE";
    public static final String DESCRIPTION = "DESCRIPTION";
    public static final String TEXT_LINK = "TEXT_LINK";
    public static final String TEXT_MAIN = "TEXT_MAIN";
    public static final String DESCRIPTION_EN = "DESCRIPTION_EN";
    public static final String TEXT_LINK_EN = "TEXT_LINK_EN";
    public static final String TEXT_MAIN_EN = "TEXT_MAIN_EN";
    public static final String WAS_OPEN = "WAS_OPEN";
    public static final String PREVIEW = "PREVIEW";
    public static final String PICTURES = "PICTURES";
    public static final String TAGS = "TAGS";
    public static final String THEMES = "THEMES";
    public static final String METRO = "METRO";
    public static final String METRO_EN = "METRO_EN";
    public static final String INVISIBLE = "INVISIBLE";
    public static final String CREATED_AT = "CREATED_AT";
    public static final String UPDATED_AT = "UPDATED_AT";

//    поля для объекта places
    @Column(name = PLACE_ID)
    private int placeId;

    @Column(name = NAME)
    private String placeName;

    @Column(name = ADDRESS)
    private String placeAddress;

    @Column(name = NAME_EN)
    private String placeNameEn;

    @Column(name = ADDRESS_EN)
    private String placeAddressEn;

    @Column(name = BLANK_ADDRESS)
    private int placeBlankAddress;

    @Column(name = LAT)
    private double placeLat;

    @Column(name = LNG)
    private double placeLng;

    @Column(name = DISTANCE)
    private double placeDisance;

    @Column(name = DESCRIPTION)
    private String placeDescription;

    @Column(name = TEXT_LINK)
    private String placeTextLink;

    @Column(name = TEXT_MAIN)
    private String placeTextMain;

    @Column(name = DESCRIPTION_EN)
    private String placeDescriptionEn;

    @Column(name = TEXT_LINK_EN)
    private String placeTextLinkEn;

    @Column(name = TEXT_MAIN_EN)
    private String placeTextMainEn;

    @Column(name = METRO)
    private String placeMetro;

    @Column(name = METRO_EN)
    private String placeMetroEn;

    @Column(name = INVISIBLE)
    private boolean placeInvisible;

    @Column(name = CREATED_AT)
    private long placeCreatedAt;

    @Column(name = UPDATED_AT)
    private long placeUpdatedAt;

    @Column(name = WAS_OPEN)
    private boolean placeWasOpen = false;

    //    полe для словаря preview
    @Column(name = PREVIEW)
    private PreviewPlace previewId;

    //    полe для словаря pictures
    @Column(name = PICTURES)
    private PicturesPlace picturesId;

    //    полe для словаря tags
    @Column(name = TAGS)
    private TagsPlace tagsId;

    //    поля для словаря themes
    @Column(name = THEMES)
    private ThemesPlace themesId;


    public Place(){
        super();
    }


    public void setPlaceId(int id ){
        this.placeId = id;
    }

    public void setPlaceName(String name){
        this.placeName = name;
    }

    public void setPlaceAddress(String address){
        this.placeAddress = address;
    }

    public void setPlaceNameEn(String nameEn){
        this.placeNameEn = nameEn;
    }

    public void setPlaceAddressEn(String addressEn){
        this.placeAddressEn = addressEn;
    }

    public void setPlaceBlankAddress(int blankAddress){
        this.placeBlankAddress = blankAddress;
    }

    public void setPlaceLat(double lat){
        this.placeLat = lat;
    }

    public void setPlaceLng(double lng){
        this.placeLng = lng;
    }

    public void setPlaceDistance(double disance){
        this.placeDisance = disance;
    }

    public void setPlaceDescription(String description){
        this.placeDescription = description;
    }

    public void setPlaceTextLink(String place_text_link){
        this.placeTextLink = place_text_link;
    }

    public void setPlaceTextMain(String textMain){
        this.placeTextMain = textMain;
    }

    public void setPlaceDescriptionEn(String descriptionEn){
        this.placeDescriptionEn = descriptionEn;
    }

    public void setPlaceTextLinkEn(String textLinkEn){
        this.placeTextLinkEn = textLinkEn;
    }

    public void setPlaceTextMainEn(String textMainEn){
        this.placeTextMainEn = textMainEn;
    }

    public void setPlaceMetro(String metro){
        this.placeMetro = metro;
    }

    public void setPlaceMetroEn(String metroEn){
        this.placeMetroEn = metroEn;
    }

    public void setPlaceInvisible(boolean invisible){
        this.placeInvisible = invisible;
    }

    public void setPlaceCreateId(long createId){
        this.placeCreatedAt = createId;
    }

    public void setPlaceUpdateAt(long updateAt){
        this.placeUpdatedAt = updateAt;
    }

    public void setPlaceWasOpen(boolean wasOpen){
        this.placeWasOpen = wasOpen;
    }

    public void setPlacePreview(PreviewPlace preview_id){
        this.previewId = preview_id;
    }

    public void setPlacePictures(PicturesPlace pictures_id){
        this.picturesId = pictures_id;
    }

    public void setPlaceTags(TagsPlace tags_id){
        this.tagsId = tags_id;
    }

    public void setPlaceThemes(ThemesPlace themes_id){
        this.themesId = themes_id;
    }


    public int getPlaceId(){
        return this.placeId;
    }

    public String getPlaceName(){
        return this.placeName;
    }

    public String getPlaceAddress(){
        return this.placeAddress;
    }

    public String getPlaceNameEn(){
        return this.placeNameEn;
    }

    public String getPlaceAddressEn(){
        return this.placeAddressEn ;
    }

    public int getPlaceBlankAddress(){
        return this.placeBlankAddress;
    }

    public double getPlaceLat(){
        return this.placeLat ;
    }

    public double getPlaceLng(){
        return this.placeLng;
    }

    public double getPlaceDistance(){
         return this.placeDisance;
    }

    public String getPlaceDescription(){
        return this.placeDescription;
    }

    public String getPlaceTextLink(){
        return this.placeTextLink;
    }

    public String getPlaceTextMain(){
        return this.placeTextMain;
    }

    public String getPlaceDescriptionEn(){
        return this.placeDescriptionEn;
    }

    public String getPlaceTextLinkEn(){
        return this.placeTextLinkEn ;
    }

    public String getPlaceTextMainEn(){
        return this.placeTextMainEn;
    }

    public String getPlaceMetro(){
        return this.placeMetro;
    }

    public String getPlaceMetroEn(){
        return this.placeMetroEn;
    }

    public boolean getPlaceInvisible(){
        return this.placeInvisible;
    }

    public long getPlaceCreateId(){
        return this.placeCreatedAt;
    }

    public long getPlaceUpdateAt(){
        return this.placeUpdatedAt;
    }

    public boolean getPlaceWasOpen(){
        return this.placeWasOpen;
    }

    public PreviewPlace getPlacePreview(){
        return this.previewId;
    }

    public PicturesPlace getPlacePictures(){
        return this.picturesId;
    }

    public TagsPlace getPlaceTags(){
        return this.tagsId;
    }

    public ThemesPlace getPlaceThemes(){
        return this.themesId;
    }


    public static List<Place> getAll(){
        return new Select().from(Place.class).execute();
    }

    public static List<Place> getAllVisible(){
        return new Select().from(Place.class).where(Place.INVISIBLE + " = ?", false).execute();
    }

    public static Place getByPlaceId(int id){
        return new Select().from(Place.class).where(PLACE_ID + " = ?", id).executeSingle();
    }

    public static Place getByPlaceName(String name){
        return new Select().from(Place.class).where(NAME + " = ?", name).executeSingle();
    }

    public static Place getByPlaceNameEn(String nameEn){
        return new Select().from(Place.class).where(NAME_EN + " = ?", nameEn).executeSingle();
    }
}
