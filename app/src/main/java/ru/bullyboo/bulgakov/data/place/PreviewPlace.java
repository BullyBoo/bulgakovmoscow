package ru.bullyboo.bulgakov.data.place;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Таблица базы данных
 */
@Table(name = "place_preview_table")
public class PreviewPlace extends Model implements Serializable{

    public static final String PREVIEW_ID = "PREVIEW_ID";
    public static final String PREVIEW_PARENT_ID = "PREVIEW_PARENT_ID";
    public static final String PREVIEW_URL = "PREVIEW_URL";

    @Column(name = PREVIEW_PARENT_ID)
    private int previewParentId;

    @Column(name = PREVIEW_ID)
    private int previewId;

    @Column(name = PREVIEW_URL)
    private String previewUrl;


    public PreviewPlace(){
        super();
    }


    public void setPreviewParentid(int id){
        this.previewParentId = id;
    }

    public void setPreviewId(int id){
        this.previewId = id;
    }

    public void setPreviewUrl(String url){
        this.previewUrl = url;
    }


    public int getPreviewParentId(){
        return this.previewParentId;
    }

    public int getPreviewId(){
        return this.previewId;
    }

    public String getPreviewUrl(){
        return this.previewUrl;
    }


    public static List<PreviewPlace> getAll(){
        return new Select().from(PreviewPlace.class).execute();
    }

    public static PreviewPlace getByParentId(int id){
        return new Select().from(PreviewPlace.class).where(PREVIEW_PARENT_ID + " = ?", id).executeSingle();
    }
}
