package ru.bullyboo.bulgakov.data.place;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

/**
 * Created by BullyBoo on 20.10.2016.
 */
@Table(name = "place_json_table")
public class PlaceJson extends Model implements Serializable {

    public static final String PLACE_JSON_VERSION = "PLACE_JSON_VERSION";
    public static final String PLACE_JSON_PLACE_ARRAY_SIZE = "PLACE_JSON_PLACE_ARRAY_SIZE";
    public static final String PLACE_JSON_LAST_UPDATE = "PLACE_JSON_LAST_UPDATE";

    @Column(name = PLACE_JSON_VERSION)
    private String placeJsonVersion;

    @Column(name = PLACE_JSON_PLACE_ARRAY_SIZE)
    private int placeJsonPlaceArraySize;

    @Column(name = PLACE_JSON_LAST_UPDATE)
    private double placeJsonLastUpdate;


    public PlaceJson(){
        super();
    }


    public void setPlaceJsonVersion(String version){
        this.placeJsonVersion = version;
    }

    public void setPlaceJsonPlaceArraySize(int arraySize){
        this.placeJsonPlaceArraySize = arraySize;
    }

    public void setPlaceJsonLastUpdate(double lastUpdate){
        this.placeJsonLastUpdate = lastUpdate;
    }


    public String getPlaceJsonVersion(){
        return this.placeJsonVersion;
    }

    public int getPlaceJsonPlaceArraySize(){
        return this.placeJsonPlaceArraySize;
    }

    public double getPlaceJsonLastUpdate(){
        return this.placeJsonLastUpdate;
    }


    public static PlaceJson getPlaceJsonInfo(){
        return new Select().from(PlaceJson.class).executeSingle();
    }
}