package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BullyBoo on 20.10.2016.
 */

@Table(name = "route_polylines_table")
public class PolylineRoute extends Model implements Serializable {

    public static final String ROUTE_POLYLINE_PARENT_ID = "ROUTE_POLYLINE_PARENT_ID";
    public static final String ROUTE_POLYLINE_LAT = "ROUTE_POLYLINE_LAT";
    public static final String ROUTE_POLYLINE_LNG = "ROUTE_POLYLINE_LNG";

    @Column(name = ROUTE_POLYLINE_PARENT_ID)
    private int routePolylineParentId;

    @Column(name = ROUTE_POLYLINE_LAT)
    private double routePolylineLat;

    @Column(name = ROUTE_POLYLINE_LNG)
    private double routePolylineLng;


    public PolylineRoute(){
        super();
    }


    public void setRoutePolylineParentId(int id){
        this.routePolylineParentId = id;
    }

    public void setRoutePolylineLat(double lat){
        this.routePolylineLat = lat;
    }

    public void setRoutePolylineLng(double lng){
        this.routePolylineLng = lng;
    }


    public int getRoutePolylineParentId(){
        return this.routePolylineParentId;
    }

    public double getRoutePolylineLat(){
        return this.routePolylineLat;
    }

    public double getRoutePolylineLng(){
        return this.routePolylineLng;
    }


    public static List<PolylineRoute> getAll(){
        return new Select().from(PolylineRoute.class).execute();
    }

    public static List<PolylineRoute> getRoutePolylineByParentId(int id){
        return new Select().from(PolylineRoute.class).where(ROUTE_POLYLINE_PARENT_ID + " = ?", id).execute();
    }
}
