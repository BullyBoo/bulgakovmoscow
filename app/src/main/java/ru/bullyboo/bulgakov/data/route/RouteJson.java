package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;

import ru.bullyboo.bulgakov.data.place.PlaceJson;

/**
 * Created by BullyBoo on 20.10.2016.
 */

@Table(name = "route_json_table")
public class RouteJson extends Model implements Serializable {

    public static final String ROUTE_JSON_VERSION = "ROUTE_JSON_VERSION";
    public static final String ROUTE_JSON_PLACE_ARRAY_SIZE = "ROUTE_JSON_PLACE_ARRAY_SIZE";
    public static final String ROUTE_JSON_LAST_UPDATE = "ROUTE_JSON_LAST_UPDATE";

    @Column(name = ROUTE_JSON_VERSION)
    private String routeJsonVersion;

    @Column(name = ROUTE_JSON_PLACE_ARRAY_SIZE)
    private int routeJsonPlaceArraySize;

    @Column(name = ROUTE_JSON_LAST_UPDATE)
    private double routeJsonLastUpdate;


    public RouteJson(){
        super();
    }


    public void setPlaceJsonVersion(String version){
        this.routeJsonVersion = version;
    }

    public void setPlaceJsonPlaceArraySize(int arraySize){
        this.routeJsonPlaceArraySize = arraySize;
    }

    public void setPlaceJsonLastUpdate(double lastUpdate){
        this.routeJsonLastUpdate = lastUpdate;
    }


    public String getPlaceJsonVersion(){
        return this.routeJsonVersion;
    }

    public int getPlaceJsonPlaceArraySize(){
        return this.routeJsonPlaceArraySize;
    }

    public double getPlaceJsonLastUpdate(){
        return this.routeJsonLastUpdate;
    }


    public static RouteJson getRouteJsonInfo(){
        return new Select().from(RouteJson.class).executeSingle();
    }
}