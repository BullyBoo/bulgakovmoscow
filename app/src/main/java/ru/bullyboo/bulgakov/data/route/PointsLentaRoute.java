package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BullyBoo on 20.10.2016.
 */

@Table(name = "route_points_lenta_table")
public class PointsLentaRoute extends Model implements Serializable {

    public static final String ROUTE_POINTS_LENTA_PARENT_ID = "ROUTE_POINTS_LENTA_PARENT_ID";
    public static final String ROUTE_POINTS_LENTA_ID = "ROUTE_POINTS_LENTA_ID";

    @Column(name = ROUTE_POINTS_LENTA_PARENT_ID)
    private int routePointsLentaParentId;

    @Column(name = ROUTE_POINTS_LENTA_ID)
    private int routePointsLentaId;


    public PointsLentaRoute(){
        super();
    }


    public void setPointsLentaParentid(int id){
        this.routePointsLentaParentId = id;
    }

    public void setPointsLentaId(int id){
        this.routePointsLentaId = id;
    }


    public int getPointsLentaParentId(){
        return this.routePointsLentaParentId;
    }

    public double getPointsLentaId(){
        return this.routePointsLentaId;
    }


    public static List<PointsLentaRoute> getAll(){
        return new Select().from(PointsLentaRoute.class).execute();
    }
}