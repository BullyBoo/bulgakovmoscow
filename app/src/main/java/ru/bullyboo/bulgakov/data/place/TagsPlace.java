package ru.bullyboo.bulgakov.data.place;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Таблица базы данных
 */
@Table(name = "place_tags_table")
public class TagsPlace extends Model implements Serializable{

    public static final String TAGS_PARENT_ID = "TAGS_PARENT_ID";
    public static final String TAGS_ID = "TAGS_ID";
    public static final String TAGS_NAME = "TAGS_NAME";
    public static final String TAGS_NAME_EN = "TAGS_NAME_EN";
    public static final String TAGS_UPDATED_AT = "TAGS_UPDATED_AT";
    public static final String TAGS_CREATED_AT = "TAGS_CREATED_AT";

    @Column(name = TAGS_PARENT_ID)
    private int tagsParentId;

    @Column(name = TAGS_ID)
    private int tagsId;

    @Column(name = TAGS_NAME)
    private String tagsName;

    @Column(name = TAGS_NAME_EN)
    private String tagsNameEn;

    @Column(name = TAGS_UPDATED_AT)
    private long tagsUpdatedAt;

    @Column(name = TAGS_CREATED_AT)
    private long tagsCreatedAt;


    public TagsPlace(){
        super();
    }


    public void setTagsParentId(int id){
        this.tagsParentId = id;
    }

    public void setTagsId(int id){
        this.tagsId = id;
    }

    public void setTagsName(String name){
        this.tagsName = name;
    }

    public void setTagsNameEn(String nameEn){
        this.tagsNameEn = nameEn;
    }

    public void setTagsUpdatedAt(long updatedAt){
        this.tagsUpdatedAt = updatedAt;
    }

    public void setTagsCreatedAt(long createdAt){
        this.tagsCreatedAt = createdAt;
    }


    public int getTagsParentId(){
        return this.tagsParentId;
    }

    public int getTagsId(){
        return this.tagsId;
    }

    public String getTagsName(){
        return this.tagsName;
    }

    public String getTagsNameEn(){
        return this.tagsNameEn;
    }

    public long getTagsUpdatedAt(){
        return this.tagsUpdatedAt;
    }

    public long getTagsCreatedAt(){
        return this.tagsCreatedAt;
    }


    public static List<TagsPlace> getAll(){
        return new Select().from(TagsPlace.class).execute();
    }

    public static List<TagsPlace> getByMainId(int mainId){
        return new Select().from(TagsPlace.class).where(TAGS_PARENT_ID + " = ?", mainId).execute();
    }

    public static List<TagsPlace> getByName(String name){
        return new Select().from(TagsPlace.class).where(TAGS_NAME + " LIKE ?", "%" + name + "%").execute();
    }
    public static List<TagsPlace> getByNameEn(String nameEn){
        return new Select().from(TagsPlace.class).where(TAGS_NAME_EN + " LIKE ?", "%" + nameEn + "%").execute();
    }
}
