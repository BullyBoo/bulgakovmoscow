package ru.bullyboo.bulgakov.data.place;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Таблица базы данных
 */
@Table(name = "place_pictures_table")
public class PicturesPlace extends Model implements Serializable{

    public static final String PICTURES_PARENT_ID = "PICTURES_PARENT_ID";
    public static final String PICTURES_ID = "PICTURES_ID";
    public static final String PICTURES_URL = "PICTURES_URL";

    @Column(name = PICTURES_PARENT_ID)
    private int picturesParentId;

    @Column(name = PICTURES_ID)
    private int picturesId;

    @Column(name = PICTURES_URL)
    private String picturesUrl;


    public PicturesPlace(){
        super();
    }


    public void setPicturesParentId(int id){
        this.picturesParentId = id;
    }

    public void setPicturesId(int id){
        this.picturesId = id;
    }

    public void setPicturesUrl(String url){
        this.picturesUrl = url;
    }


    public int getPicturesParentId(){
        return this.picturesParentId;
    }

    public int getPicturesId(){
        return this.picturesId;
    }

    public String getPicturesUrl(){
        return this.picturesUrl;
    }


    public static List<PicturesPlace> getAll(){
        return new Select().from(PicturesPlace.class).execute();
    }

    public static List<PicturesPlace> getByParentId(int id){
        return new Select().from(PicturesPlace.class).where(PICTURES_PARENT_ID + " = ?", id).execute();
    }
}
