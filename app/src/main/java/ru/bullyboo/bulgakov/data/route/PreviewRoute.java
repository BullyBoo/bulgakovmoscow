package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BullyBoo on 20.10.2016.
 */
@Table(name = "routes_preview_table")
public class PreviewRoute extends Model implements Serializable{

    public static final String ROUTE_PREVIEW_PARENT_ID = "ROUTE_PREVIEW_PARENT_ID";
    public static final String ROUTE_PREVIEW_ID = "ROUTE_PREVIEW_ID";
    public static final String ROUTE_PREVIEW_URL= "ROUTE_PREVIEW_URL";

    @Column(name = ROUTE_PREVIEW_PARENT_ID)
    private int routePreviewParentId;

    @Column(name = ROUTE_PREVIEW_ID)
    private int routePreviewId;

    @Column(name = ROUTE_PREVIEW_URL)
    private String routePreviewUrl;


    public PreviewRoute(){
        super();
    }


    public void setRoutePreviewParentId(int id){
        this.routePreviewParentId = id;
    }

    public void setRoutePreviewId(int id){
        this.routePreviewId = id;
    }

    public void setRoutePreviewUrl(String url){
        this.routePreviewUrl = url;
    }


    public int getRoutePreviewParentId(){
        return this.routePreviewParentId;
    }

    public int getRoutePreviewId(){
        return this.routePreviewId;
    }

    public String getRoutePreviewUrl(){
        return this.routePreviewUrl;
    }


    public static List<PreviewRoute> getAll(){
        return new Select().from(PreviewRoute.class).execute();
    }

    public static PreviewRoute getPreviewRouteByParentId(int id){
        return new Select().from(PreviewRoute.class).where(ROUTE_PREVIEW_PARENT_ID + " = ?", id).executeSingle();
    }
}
