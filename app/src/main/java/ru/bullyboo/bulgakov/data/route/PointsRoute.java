package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BullyBoo on 20.10.2016.
 */

@Table(name = "route_points_table")
public class PointsRoute extends Model implements Serializable {

    public static final String ROUTE_POINTS_PARENT_ID = "ROUTE_POINTS_PARENT_ID";
    public static final String ROUTE_POINTS_ID = "ROUTE_POINTS_ID";

    @Column(name = ROUTE_POINTS_PARENT_ID)
    private int routePointsParentId;

    @Column(name = ROUTE_POINTS_ID)
    private int routePointsId;


    public PointsRoute(){
        super();
    }


    public void setPointsParentid(int id){
        this.routePointsParentId = id;
    }

    public void setPointsId(int id){
        this.routePointsId = id;
    }


    public int getPointsParentId(){
        return this.routePointsParentId;
    }

    public int getPointsId(){
        return this.routePointsId;
    }


    public static List<PointsRoute> getAll(){
        return new Select().from(PointsRoute.class).execute();
    }

    public static List<PointsRoute> getPointsRouteByParentId(int id){
        return new Select().from(PointsRoute.class).where(ROUTE_POINTS_PARENT_ID + " = ?", id).execute();
    }
}