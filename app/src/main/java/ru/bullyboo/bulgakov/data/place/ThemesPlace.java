package ru.bullyboo.bulgakov.data.place;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Таблица базы данных
 */
@Table(name = "place_themes_table")
public class ThemesPlace extends Model implements Serializable{

    //        Адреса Булгакова - 2
    public static final int ADDRESS_BULGAKOV = 2;
    //        Личная жизнь - 14
    public static final int PERSONAL_LIFE = 14;
    //        Писатель и власть - 16
    public static final int WRITER_AND_GOVERMENT = 16;
    //        Проза - 15
    public static final int PROSE = 15;
    //        Театр - 6
    public static final int THEATER = 6;


    public static final String THEMES_ID = "THEMES_ID";
    public static final String THEMES_PARENT_ID = "THEMES_PARENT_ID";
    public static final String THEMES_NAME = "THEMES_NAME";
    public static final String THEMES_NAME_EN = "THEMES_NAME_EN";
    public static final String THEMES_UPDATED_AT = "THEMES_UPDATED_AT";
    public static final String THEMES_CREATED_AT = "THEMES_CREATED_AT";

    @Column( name = THEMES_PARENT_ID)
    private int themesParentId;

    @Column( name = THEMES_ID)
    private int themesId;

    @Column(name = THEMES_NAME)
    private String themesName;

    @Column(name = THEMES_NAME_EN)
    private String themesNameEn;

    @Column(name = THEMES_UPDATED_AT)
    private long themesUpdatedAt;

    @Column(name = THEMES_CREATED_AT)
    private long themesCreatedAt;


    public ThemesPlace(){
        super();
    }


    public void setThemesParentId(int id){
        this.themesParentId = id;
    }

    public void setThemesId(int id){
        this.themesId = id;
    }

    public void setThemesName(String name){
        this.themesName = name;
    }

    public void setThemesNameEn(String nameEn){
        this.themesNameEn = nameEn;
    }

    public void setThemesUpdateAt(long updateAt){
        this.themesUpdatedAt = updateAt;
    }

    public void setThemesCreatedAt(long createdAt){
        this.themesCreatedAt = createdAt;
    }


    public int getThemesParentId(){
        return this.themesParentId;
    }

    public int getThemesId(){
        return this.themesId;
    }

    public String getThemesName(){
        return this.themesName;
    }

    public String getThemesNameEn(){
        return this.themesNameEn;
    }

    public long getThemesUpdateAt(){
        return this.themesUpdatedAt;
    }

    public long getThemesCreatedAt(){
        return this.themesCreatedAt;
    }


    public static List<ThemesPlace> getAll(){
        return new Select().from(ThemesPlace.class).execute();
    }

    public static List<ThemesPlace> getById(int id){
        return new Select().from(ThemesPlace.class).where(THEMES_ID + " = ?", id).execute();
    }
}
