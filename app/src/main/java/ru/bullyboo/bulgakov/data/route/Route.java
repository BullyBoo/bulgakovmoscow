package ru.bullyboo.bulgakov.data.route;

import com.activeandroid.Model;
import com.activeandroid.annotation.Column;
import com.activeandroid.annotation.Table;
import com.activeandroid.query.Select;

import java.io.Serializable;
import java.util.List;

/**
 * Created by BullyBoo on 20.10.2016.
 */
@Table(name = "routes_table")
public class Route extends Model implements Serializable {

    public static final String ROUTE_ID = "ROUTES_ID";
    public static final String ROUTE_UPDATE_AT = "ROUTES_UPDATE_AT";
    public static final String ROUTE_NAME = "ROUTES_NAME";
    public static final String ROUTE_NAME_EN = "ROUTES_NAME_EN";
    public static final String ROUTE_BLANK_ADDRESS = "ROUTES_BLANK_ADDRESS";
    public static final String ROUTE_DISTANCE = "ROUTES_DISTANCE";
    public static final String ROUTE_DURATION = "ROUTES_DURATION";
    public static final String ROUTE_DESCRIPTION = "ROUTES_DESCRIPTION";
    public static final String ROUTE_TEXT_MAIN = "ROUTES_TEXT_MAIN";
    public static final String ROUTE_DESCRIPTION_EN = "ROUTES_DESCRIPTION_EN";
    public static final String ROUTE_TEXT_MAIN_EN = "ROUTES_TEXT_MAIN_EN";
    public static final String ROUTE_THEME = "ROUTE_THEME";
    public static final String ROUTE_LAST_UPDATE = "ROUTES_LAST_UPDATE";

    @Column(name = ROUTE_ID)
    private int routeId;

    @Column(name = ROUTE_UPDATE_AT)
    private long routeUpdateAt;

    @Column(name = ROUTE_NAME)
    private String routeName;

    @Column(name = ROUTE_NAME_EN)
    private String routeNameEn;

    @Column(name = ROUTE_BLANK_ADDRESS)
    private int routeBlankAddress;

    @Column(name = ROUTE_DISTANCE)
    private String routeDistance;

    @Column(name = ROUTE_DURATION)
    private String routeDuration;

    @Column(name = ROUTE_DESCRIPTION)
    private String routeDescription;

    @Column(name = ROUTE_TEXT_MAIN)
    private String routeTextMain;

    @Column(name = ROUTE_DESCRIPTION_EN)
    private String routeDescriptionEn;

    @Column(name = ROUTE_TEXT_MAIN_EN)
    private String routeTextMainEn;

    @Column(name = ROUTE_THEME)
    private int routeTheme;

    @Column(name = ROUTE_LAST_UPDATE)
    private long routeLastUpdate;


    public Route(){
        super();
    }


    public void setRouteId(int id){
        this.routeId = id;
    }

    public void setRouteUpdateAt(long updateAt){
        this.routeUpdateAt = updateAt;
    }

    public void setRouteName(String name){
        this.routeName = name;
    }

    public void setRouteNameEn(String nameEn){
        this.routeNameEn = nameEn;
    }

    public void setRouteBlankAddress(int blankAddress){
        this.routeBlankAddress = blankAddress;
    }

    public void setRouteDistance(String distance){
        this.routeDistance = distance;
    }

    public void setRouteDuration(String duaration){
        this.routeDuration = duaration;
    }

    public void setRouteDescription(String description){
        this.routeDescription = description;
    }

    public void setRouteTextMain(String textMain){
        this.routeTextMain = textMain;
    }

    public void setRouteDescriptionEn(String descriptionEn){
        this.routeDescriptionEn = descriptionEn;
    }

    public void setRouteTextMainEn(String textMainEn){
        this.routeTextMainEn = textMainEn;
    }

    public void setRouteTheme(int theme){
        this.routeTheme = theme;
    }

    public void setRouteLastUpdate(long lastUpdate){
        this.routeLastUpdate = lastUpdate;
    }


    public int getRouteId(){
        return this.routeId;
    }

    public long getRouteUpdateAt(){
        return this.routeUpdateAt;
    }

    public String getRouteName(){
        return this.routeName;
    }

    public String getRouteNameEn(){
        return this.routeNameEn;
    }

    public int getRouteBlankAddress(){
        return this.routeBlankAddress;
    }

    public String getRouteDistance(){
        return this.routeDistance;
    }

    public String getRouteDuration(){
        return this.routeDuration;
    }

    public String getRouteDescription(){
        return this.routeDescription;
    }

    public String getRouteTextMain(){
        return this.routeTextMain;
    }

    public String getRouteDescriptionEn(){
        return this.routeDescriptionEn;
    }

    public String getRouteTextMainEn(){
        return this.routeTextMainEn;
    }

    public int getRouteTheme(){
        return this.routeTheme;
    }

    public long getRouteLastUpdate(){
        return this.routeLastUpdate;
    }


    public static List<Route> getAll(){
        return new Select().from(Route.class).execute();
    }

    public static Route getRouteById(int id){
        return new Select().from(Route.class).where(ROUTE_ID + " = ?", id).executeSingle();
    }
}
