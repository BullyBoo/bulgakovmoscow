package ru.bullyboo.bulgakov.ui;

import android.app.Dialog;
import android.content.Context;
import android.util.Log;
import android.view.Window;

import ru.bullyboo.bulgakov.R;

/**
 * Кастомный диалог
 */
public class CustomDialog {

    private Dialog dialog;

    public CustomDialog(Context context){

        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.dialog_download);

        dialog.setCancelable(false);

        dialog.show();
    }

    public void close(){
        dialog.cancel();
    }
}
