package ru.bullyboo.bulgakov.ui;

import android.app.Dialog;
import android.content.Context;
import android.os.Handler;
import android.view.View;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ProgressBar;

import ru.bullyboo.bulgakov.R;

/**
 * Created by BullyBoo on 24.10.2016.
 */

public class FirstStartDialog {

    private Context context;

    private Dialog dialog;

    private CheckBox checkBox1, checkBox2, checkBox3, checkBox4, checkBox5, checkBox6;
    private ProgressBar progress1, progress2, progress3, progress4, progress5, progress6;

    public FirstStartDialog(Context context){
        this.context = context;

        initialization();

        dialog.show();
    }

    private void initialization(){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(R.layout.first_start_dialog);

        dialog.setCancelable(false);

        checkBox1 = (CheckBox) dialog.findViewById(R.id.checkBox1);
        checkBox2 = (CheckBox) dialog.findViewById(R.id.checkBox2);
        checkBox3 = (CheckBox) dialog.findViewById(R.id.checkBox3);
        checkBox4 = (CheckBox) dialog.findViewById(R.id.checkBox4);
        checkBox5 = (CheckBox) dialog.findViewById(R.id.checkBox5);
        checkBox6 = (CheckBox) dialog.findViewById(R.id.checkBox6);

        checkBox1.setClickable(false);
        checkBox2.setClickable(false);
        checkBox3.setClickable(false);
        checkBox4.setClickable(false);
        checkBox5.setClickable(false);
        checkBox6.setClickable(false);

        progress1 = (ProgressBar) dialog.findViewById(R.id.progressBar1);
        progress2 = (ProgressBar) dialog.findViewById(R.id.progressBar2);
        progress3 = (ProgressBar) dialog.findViewById(R.id.progressBar3);
        progress4 = (ProgressBar) dialog.findViewById(R.id.progressBar4);
        progress5 = (ProgressBar) dialog.findViewById(R.id.progressBar5);
        progress6 = (ProgressBar) dialog.findViewById(R.id.progressBar6);

    }

    public void setCheckBox1Text(String text){
        checkBox1.setText(text);
    }

    public void setCheckBox2Text(String text){
        checkBox2.setText(text);
    }

    public void setCheckBox3Text(String text){
        checkBox3.setText(text);
    }

    public void setCheckBox4Text(String text){
        checkBox4.setText(text);
    }

    public void setCheckBox5Text(String text){
        checkBox5.setText(text);
    }

    public void setCheckBox6Text(String text){
        checkBox6.setText(text);
    }



    public void setCheckBox1Checked(boolean checked){
        checkBox1.setChecked(checked);
    }

    public void setCheckBox2Checked(boolean checked){
        checkBox2.setChecked(checked);
    }

    public void setCheckBox3Checked(boolean checked){
        checkBox3.setChecked(checked);
    }

    public void setCheckBox4Checked(boolean checked){
        checkBox4.setChecked(checked);
    }

    public void setCheckBox5Checked(boolean checked){
        checkBox5.setChecked(checked);
    }

    public void setCheckBox6Checked(boolean checked){
        checkBox6.setChecked(checked);
    }



    public void setProgreeBar1Visible(boolean visible){
        if(visible)
            progress1.setVisibility(View.VISIBLE);
        else
            progress1.setVisibility(View.INVISIBLE);
    }

    public void setProgreeBar2Visible(boolean visible){
        if(visible)
            progress2.setVisibility(View.VISIBLE);
        else
            progress2.setVisibility(View.INVISIBLE);
    }

    public void setProgreeBar3Visible(boolean visible){
        if(visible)
            progress3.setVisibility(View.VISIBLE);
        else
            progress3.setVisibility(View.INVISIBLE);
    }

    public void setProgreeBar4Visible(boolean visible){
        if(visible)
            progress4.setVisibility(View.VISIBLE);
        else
            progress4.setVisibility(View.INVISIBLE);
    }

    public void setProgreeBar5Visible(boolean visible){
        if(visible)
            progress5.setVisibility(View.VISIBLE);
        else
            progress5.setVisibility(View.INVISIBLE);
    }

    public void setProgreeBar6Visible(boolean visible){
        if(visible)
            progress6.setVisibility(View.VISIBLE);
        else
            progress6.setVisibility(View.INVISIBLE);
    }



    public void close(){
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                dialog.cancel();
            }
        }, 2000);
    }
}
