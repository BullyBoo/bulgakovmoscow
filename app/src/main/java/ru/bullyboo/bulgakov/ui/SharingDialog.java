package ru.bullyboo.bulgakov.ui;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.tools.ShareHelper;
import ru.bullyboo.bulgakov.tools.TypeFaceHelper;

/**
 * Created by BullyBoo on 25.10.2016.
 */

public class SharingDialog implements View.OnClickListener{

    private Dialog dialog;

    private Context context;
    private Activity activity;

    private TextView facebook, other;

    private Place place;

    public SharingDialog(Context context, Activity activity, Place place){
        this.context = context;
        this.activity = activity;
        this.place = place;

        init();
    }

    private void init(){
        dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        dialog.setContentView(R.layout.dialog_share);

        facebook = (TextView) dialog.findViewById(R.id.share_facebook);
        other = (TextView) dialog.findViewById(R.id.share_other);

        TypeFaceHelper.setDescriptionTypeFace(context, facebook);
        TypeFaceHelper.setDescriptionTypeFace(context, other);

        facebook.setOnClickListener(this);
        other.setOnClickListener(this);

        dialog.show();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.share_facebook:
                ShareHelper shareHelper = new ShareHelper(context, activity);
                shareHelper.sendShare(shareHelper.createShared(place));
                break;
            case R.id.share_other:
                Intent sendIntent = new Intent();
                sendIntent.setAction(Intent.ACTION_SEND);
                sendIntent.putExtra(Intent.EXTRA_TEXT, "https://play.google.com/store/apps/details?id=com.mapbox.mapboxsdk.android.testapp");
                sendIntent.setType("text/plain");
                context.startActivity(sendIntent);
                break;
        }
    }
}
