package ru.bullyboo.bulgakov;

import android.app.Application;

import com.activeandroid.ActiveAndroid;
import com.activeandroid.Configuration;

import com.crashlytics.android.Crashlytics;
import io.fabric.sdk.android.Fabric;
import ru.bullyboo.bulgakov.data.place.PicturesPlace;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.PlaceJson;
import ru.bullyboo.bulgakov.data.place.PreviewPlace;
import ru.bullyboo.bulgakov.data.place.TagsPlace;
import ru.bullyboo.bulgakov.data.place.ThemesPlace;
import ru.bullyboo.bulgakov.data.route.PointsLentaRoute;
import ru.bullyboo.bulgakov.data.route.PointsRoute;
import ru.bullyboo.bulgakov.data.route.PolylineRoute;
import ru.bullyboo.bulgakov.data.route.PreviewRoute;
import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.data.route.RouteJson;
import ru.bullyboo.bulgakov.data.route.TagsRoute;

/**
 * Created by BullyBoo on 10.08.2016.
 */
public class MyApplication extends Application{

    @Override
    public void onCreate() {
        super.onCreate();
        Fabric.with(this, new Crashlytics());

//        инициализируем базу данных

        Configuration.Builder configurationBuilder = new Configuration.Builder(
                this);
        configurationBuilder.addModelClass(PicturesPlace.class);
        configurationBuilder.addModelClass(Place.class);
        configurationBuilder.addModelClass(PlaceJson.class);
        configurationBuilder.addModelClass(PreviewPlace.class);
        configurationBuilder.addModelClass(TagsPlace.class);
        configurationBuilder.addModelClass(ThemesPlace.class);
        configurationBuilder.addModelClass(PointsLentaRoute.class);
        configurationBuilder.addModelClass(PointsRoute.class);
        configurationBuilder.addModelClass(PolylineRoute.class);
        configurationBuilder.addModelClass(PreviewRoute.class);
        configurationBuilder.addModelClass(Route.class);
        configurationBuilder.addModelClass(RouteJson.class);
        configurationBuilder.addModelClass(TagsRoute.class);

        ActiveAndroid.initialize(configurationBuilder.create());
    }

}
