package ru.bullyboo.bulgakov.tools;

import android.content.Context;
import android.content.SharedPreferences;
import android.location.Location;
import android.location.LocationManager;

/**
 * Класс-помощник
 * Хранит основные данные и настройки приложения
 *
 */
public class PreferencesHelper{

    private SharedPreferences shared;

    public static final String SHARED_NAME = "SHARED_NAME";

    public static final String FIRST_START_APP = "FIRST_START_APP";

    public static final String APPLICATION_LOCAL = "APPLICATION_LOCAL";
    public static final String LOCAL_RU = "RU";
    public static final String LOCAL_EN = "RN";

    public static final String OFFLINE_MAP = "OFFLINE_MAP";
    public static final String CANCEL_TO_DOWNLOAD_OFFLINE_MAP = "CANCEL_TO_DOWNLOAD_OFFLINE_MAP";
    public static final String DOWNLOAD = "DOWNLOAD";

    public static final String LAST_LOCATION_LATITUDE = "LAST_LOCATION_LATITUDE";
    public static final String LAST_LOCATION_LONGITUDE = "LAST_LOCATION_LONGITUDE";
    public static final String MARKER_TITLE = "MARKER_TITLE";

    public static final String PLACE_LOCATION_LATITUDE = "PLACE_LOCATION_LATITUDE";
    public static final String PLACE_LOCATION_LONGITUDE = "PLACE_LOCATION_LONGITUDE";

    public static final String ROUTE_ID = "ROUTE_ID";

    public PreferencesHelper(Context context){
        shared = context.getSharedPreferences(SHARED_NAME, Context.MODE_PRIVATE);
    }

//    методы получения и сохранения флага первого запуска приложения
    public void saveFirstStartApp(){
        SharedPreferences.Editor editor = shared.edit();
        editor.putBoolean(FIRST_START_APP, false);
        editor.commit();
    }
    public boolean getFirstStartApp(){
        return shared.getBoolean(FIRST_START_APP, false);
    }

    public boolean hasFirstStartApp(){
        return shared.contains(FIRST_START_APP);
    }

    public void removeFirstStartApp(){
        SharedPreferences.Editor editor = shared.edit();
        editor.remove(FIRST_START_APP);
        editor.commit();
    }


//    методы получения и сохранения выбранного языка
    public void saveLocal(String local){
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(APPLICATION_LOCAL, local);
        editor.commit();
    }
    public String getLocal(){
        return shared.getString(APPLICATION_LOCAL, null);
    }

    public boolean hasLocal(){
        return shared.contains(APPLICATION_LOCAL);
    }



//    методы получения и сохранения флага загрузки оффлайн карт
    public void saveMap(String flag){
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(OFFLINE_MAP, flag);
        editor.commit();
    }
    public String getMap(){
        return shared.getString(OFFLINE_MAP, null);
    }

    public boolean hasMap(){
        return shared.contains(OFFLINE_MAP);
    }



//    методы получения и сохранения последних координат
    public void saveLocation(double lat, double lng){
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(LAST_LOCATION_LATITUDE, lat + "");
        editor.putString(LAST_LOCATION_LONGITUDE, lng + "");
        editor.commit();
    }
    public Location getLocation(){

        Location location = new Location(LocationManager.GPS_PROVIDER);

        location.setLatitude(Double.parseDouble(shared.getString(LAST_LOCATION_LATITUDE, null)));
        location.setLongitude(Double.parseDouble(shared.getString(LAST_LOCATION_LONGITUDE, null)));

        return location;
    }

    public boolean hasLocation(){
        if(shared.contains(LAST_LOCATION_LATITUDE) &&
                shared.contains(LAST_LOCATION_LONGITUDE))
            return true;
        else return false;
    }


//    методы получения и сохранения выбранного места
    public void savePlaceLocation(double lat, double lng, String title){
        SharedPreferences.Editor editor = shared.edit();
        editor.putString(PLACE_LOCATION_LATITUDE, lat + "");
        editor.putString(PLACE_LOCATION_LONGITUDE, lng + "");
        editor.putString(MARKER_TITLE, title);
        editor.commit();
    }

    public Location getPlaceLocation(){

        Location location = new Location(LocationManager.GPS_PROVIDER);

        location.setLatitude(Double.parseDouble(shared.getString(PLACE_LOCATION_LATITUDE, null)));
        location.setLongitude(Double.parseDouble(shared.getString(PLACE_LOCATION_LONGITUDE, null)));

        return location;
    }

    public String getPlaceLocationMarkerTitle(){
        return shared.getString(MARKER_TITLE, null);
    }

    public boolean hasPlaceLocation(){
        if(shared.contains(PLACE_LOCATION_LATITUDE) &&
                shared.contains(PLACE_LOCATION_LONGITUDE)&&
                        shared.contains(MARKER_TITLE))
            return true;
        else return false;
    }

    public void removePlaceLocation(){
        SharedPreferences.Editor editor = shared.edit();
        editor.remove(PLACE_LOCATION_LATITUDE);
        editor.remove(PLACE_LOCATION_LONGITUDE);
        editor.remove(MARKER_TITLE);
        editor.commit();
    }


    //    методы получения и сохранения выбранного места
    public void saveRouteId(int id){
        SharedPreferences.Editor editor = shared.edit();
        editor.putInt(ROUTE_ID, id);
        editor.commit();
    }
    public int getRouteId(){
        return shared.getInt(ROUTE_ID, 0);
    }

    public boolean hasRouteId(){
        if(shared.contains(ROUTE_ID))
            return true;
        else
            return false;
    }

    public void removeRouteId(){
        SharedPreferences.Editor editor = shared.edit();
        editor.remove(ROUTE_ID);
        editor.commit();
    }
}
