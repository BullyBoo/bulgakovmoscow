package ru.bullyboo.bulgakov.tools;

import android.content.Context;

import java.util.Locale;

/**
 * Класс помощник, меняющий язык по умолчанию
 */
public class LocaleHelper {

    public LocaleHelper(Context context, String locale){
        Locale newLocale = new Locale(locale);
        Locale.setDefault(newLocale);
        android.content.res.Configuration config = new android.content.res.Configuration();
        config.locale = newLocale;
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());
    }
}
