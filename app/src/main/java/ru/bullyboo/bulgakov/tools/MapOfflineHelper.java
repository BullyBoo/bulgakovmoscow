package ru.bullyboo.bulgakov.tools;

import android.content.Context;
import android.content.DialogInterface;
import android.util.Log;
import android.widget.Toast;

import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.offline.OfflineManager;
import com.mapbox.mapboxsdk.offline.OfflineRegion;
import com.mapbox.mapboxsdk.offline.OfflineRegionError;
import com.mapbox.mapboxsdk.offline.OfflineRegionStatus;
import com.mapbox.mapboxsdk.offline.OfflineTilePyramidRegionDefinition;

import org.json.JSONObject;

import ru.bullyboo.bulgakov.R;

/**
 * Данный класс осуществляет загрузку карт для оффлайн работы
 *
 */
public class MapOfflineHelper {

    private Context context;
    private MapboxMap map;
    private PreferencesHelper helper;

    private final static String JSON_CHARSET = "UTF-8";
    private final static String JSON_FIELD_REGION_NAME = "FIELD_REGION_NAME";

    private OfflineManager offlineManager;
    private OfflineRegion offlineRegion;

    public MapOfflineHelper(final Context context, MapboxMap map){
        this.context = context;
        this.map = map;

        helper = new PreferencesHelper(context);

        CheckInternetConnection checkInternetConnection =
                new CheckInternetConnection(context);

//        TODO отключить диалог
        if(checkInternetConnection.isOnline()){
            showDialog();
        }else{
            Toast.makeText(context
                    , context.getResources().getString(R.string.no_internet_connection_first_start)
                    , Toast.LENGTH_LONG)
                    .show();
        }
    }

    private void showDialog(){
        android.app.AlertDialog.Builder builder = new android.app.AlertDialog.Builder(context);

        builder.setTitle(context.getResources().getString(R.string.map_dialog_title))
                .setMessage(context.getResources().getString(R.string.map_dialog_message));

        builder.setPositiveButton(context.getResources().getString(R.string.download_dialog)
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        CheckInternetConnection checkInternetConnection =
                                new CheckInternetConnection(context);

                        if(checkInternetConnection.isOnline()){
                            saveMap(MapOfflineHelper.this.map);
                        } else{
                            Toast.makeText(context
                                    , context.getResources().getString(R.string.no_internet_connection_first_start)
                                    , Toast.LENGTH_LONG)
                                    .show();
                        }
                        dialog.cancel();
                    }
                });
        builder.setNegativeButton(context.getResources().getString(R.string.cancel_dialog)
                , new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();

                        helper.saveMap(PreferencesHelper.CANCEL_TO_DOWNLOAD_OFFLINE_MAP);
                    }
                });
        builder.setCancelable(false);

        android.app.AlertDialog alertDialog = builder.create();
        alertDialog.show();
    }

    public  void saveMap(MapboxMap map){
        offlineManager = OfflineManager.getInstance(context);

//        указываем параметры карты, которую нужно сохранить
        String styleURL = map.getStyleUrl();

        LatLngBounds latLngBounds = new LatLngBounds.Builder()
                .include(new LatLng(55.942856, 37.584687)) // Northeast
                .include(new LatLng(55.520535, 37.621561)) // Southwest
                .build();

        double minZoom = map.getCameraPosition().zoom;
        double maxZoom = map.getMaxZoom();

        float pixelRatio = context.getResources().getDisplayMetrics().density;

        OfflineTilePyramidRegionDefinition definition = new OfflineTilePyramidRegionDefinition(
                styleURL, latLngBounds, minZoom, maxZoom, pixelRatio);


//        создаем метадату для оффлайн карт
        byte[] metadata;
        try {
            JSONObject jsonObject = new JSONObject();
            jsonObject.put(JSON_FIELD_REGION_NAME, "Moscow");
            String json = jsonObject.toString();
            metadata = json.getBytes(JSON_CHARSET);
        } catch (Exception e) {
            metadata = null;
        }

        offlineManager.createOfflineRegion(definition, metadata, new OfflineManager.CreateOfflineRegionCallback() {
            @Override
            public void onCreate(OfflineRegion offlineRegion) {
                MapOfflineHelper.this.offlineRegion = offlineRegion;
                launchMap();

            }

            @Override
            public void onError(String error) {
                Log.e("myLog", "Ошибка: " + error);
            }
        });
    }

    private void launchMap(){
        offlineRegion.setObserver(new OfflineRegion.OfflineRegionObserver() {
            @Override
            public void onStatusChanged(OfflineRegionStatus status) {
                if (status.isComplete()) {
//                    карты загружены, показываем Toast
                    Toast.makeText(context,
                            context.getResources().getString(R.string.map_offline_ready),
                            Toast.LENGTH_LONG).show();

//                    сохраняем флаг, что карты уже загружены
                    helper.saveMap(PreferencesHelper.DOWNLOAD);
                    return;
                }

//                метод getCompletedResourceSize() возвращает количество байт, которые необходимо загрузить
//                переводим этот размер в мегабайты и выводим в диалог
                long size = (status.getCompletedResourceSize()/1024)/1024;

                Log.d("myLog", String.valueOf(status.getCompletedResourceCount())
                        + "/"
                        + String.valueOf(status.getRequiredResourceCount())
                        + context.getResources().getString(R.string.offline_res)
                        + "\n\n"
                        + size
                        + context.getResources().getString(R.string.offline_mb)
                        + "\n\n"
                        + context.getResources().getString(R.string.offline_wait));
            }

            @Override
            public void onError(OfflineRegionError error) {
                Log.e("myLog", "onError reason: " + error.getReason());
                Log.e("myLog", "onError message: " + error.getMessage());
            }

            @Override
            public void mapboxTileCountLimitExceeded(long limit) {
                Log.e("myLog", "Mapbox tile count limit exceeded: " + limit);
            }
        });

        offlineRegion.setDownloadState(OfflineRegion.STATE_ACTIVE);
    }

}
