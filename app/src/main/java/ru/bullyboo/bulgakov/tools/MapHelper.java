package ru.bullyboo.bulgakov.tools;

import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.location.Location;
import android.location.LocationManager;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.InfoWindow;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.util.ArrayList;
import java.util.List;

import ru.bullyboo.bulgakov.activities.MainActivity;
import ru.bullyboo.bulgakov.activities.MarkerActivity;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.route.PointsRoute;
import ru.bullyboo.bulgakov.data.route.PolylineRoute;
import ru.bullyboo.bulgakov.data.route.Route;
import ru.bullyboo.bulgakov.interfaces.MapState;

/**
 * Данный класс облегчает работу с картами:
 * ......задает изначальные настройки карты
 * ......меняет положение камеры,
 * ......устанавливает слежку за положением пользователя,
 * ......строит маршруты
 * ......настраивает окно InfoWindow
 * ......отслеживает касания, нажатия на маркеры, на карту
 * ......слушает изменение координат пользователя
 *
 */
public class MapHelper implements MapboxMap.OnMyLocationChangeListener {

    private Context context;
    private Activity activity;

    private MapView mapView;
    private MapboxMap map;

    private List<Place> places_list;

    private PreferencesHelper helper;

    private Fragment thisFragment;

    private MapState state;

    private View infoWindow;

//    флаг, по которому MapFragment узнает были ли добавлены маркеры на карту
    public boolean markersWasAdd = false;

    public MapHelper(Context context, Activity activity
            , MapView mapView, MapboxMap map, Fragment fragment, MapState state){
        this.context = context;
        this.activity = activity;
        this.mapView = mapView;
        this.map = map;
        this.thisFragment = fragment;
        this.state = state;

        helper = new PreferencesHelper(context);

        initMap();

//        ставим checkBox true, убираем progressBar, меняем и текст и закрываем диалог
        ((MainActivity) activity)
                .dialog.setCheckBox4Text(context
                .getResources()
                .getString(R.string.check_box41));
        ((MainActivity) activity)
                .dialog.setCheckBox4Checked(true);
        ((MainActivity) activity)
                .dialog.setProgreeBar4Visible(false);
        ((MainActivity) activity)
                .dialog.close();
    }
//    в данном методе происходит первоначальная настройка карты
    private void initMap(){
//        проверяем загружены ли карты для оффлайн работы
//        елси нет, предлагаем пользователю загрузить их
        if(!new PreferencesHelper(context).hasMap()){
            new MapOfflineHelper(context, map);
        }

        map.setOnMyLocationChangeListener(MapHelper.this);

//        настраиваем иконку позиции пользователя
        Drawable iconDrawable = ContextCompat.getDrawable(context, R.mipmap.direction_arrow);
        map.getMyLocationViewSettings().setForegroundDrawable(iconDrawable, iconDrawable);

//        убираем accuracy вокруг иконки пользователя
        map.getMyLocationViewSettings().setAccuracyAlpha(0);

//        отключаем компас
        map.getUiSettings().setCompassEnabled(false);

//        отключаем лого Mapbox
        map.getUiSettings().setLogoEnabled(false);

//        отключаем кнопку info, встроенную в карты
        map.getUiSettings().setAttributionEnabled(false);

//        отключаем изменение угла обзора на карту
        map.getUiSettings().setTiltGesturesEnabled(false);

//        отключаем вращение карты (карта всегда смотрит на север)
        map.getUiSettings().setRotateGesturesEnabled(false);

//        получаем последние известные координаты из PreferencesHelper
//        если их нет, ставим позицию по умолчанию на Музей Булгакова
        map.setMyLocationEnabled(true);

        Location location = map.getMyLocation();
        Log.d("myLog", "location = " + location);

        boolean centralMapOnAllMarkers = true;
        if(location == null){
            if(helper.hasLocation()){
                location = helper.getLocation();
                map.getMyLocation().set(location);
                Log.d("myLog", "location1 = " + location);
            }else{
                location = new Location(LocationManager.GPS_PROVIDER);

                Place p = Place.getByPlaceId(5);
                if(p!= null){
                    location.setLongitude(p.getPlaceLng());
                    location.setLatitude(p.getPlaceLat());
                    Log.d("myLog", "location3 = " + location);
                }else{
                    Toast.makeText(context,
                            context.getResources().getString(R.string.error),
                            Toast.LENGTH_LONG).show();
                    return;
                }
            }
        }else{
            centralMapOnAllMarkers = false;
            Log.d("myLog", "location != null");
            helper.saveLocation(location.getLatitude(), location.getLongitude());

//            устанавливаем myLocation на ту же координату, что и камеру
            this.onMyLocationChange(location);
        }

//        анимация приближения камеры к полученным координатам
        if(helper.hasPlaceLocation()){
            updatePlaceCameraPosition(helper.getPlaceLocation());
        }else{
            map.animateCamera(CameraUpdateFactory
                    .newCameraPosition(new CameraPosition.Builder()
                            .target(new LatLng(location))
                            .zoom(12)
                            .bearing(360)
                            .build()), 200);

        }

//        добавляем маркеры
        addMarkersOnMap(Place.getAll(), centralMapOnAllMarkers);

        state.mapWasDownload();
    }

    public void updateCameraPosition(){
        updateCameraPosition(map.getMyLocation(), map.getCameraPosition().zoom);
    }

    private void updateCameraPosition(Location location, double zoom){
        if(location != null) {

            map.animateCamera(CameraUpdateFactory
                    .newCameraPosition(new CameraPosition.Builder()
                            .target(new LatLng(location))
                            .zoom(zoom)
                            .bearing(360)
                            .build()), 1500);
        }
    }

    public void updatePlaceCameraPosition(Location location){
        map.animateCamera(CameraUpdateFactory
                .newCameraPosition(new CameraPosition.Builder()
                        .target(new LatLng(location))
                        .zoom(15)
                        .bearing(360)
                        .build()), 1000);
    }

    public void addMarkersOnMap(List<Place> places, boolean centralMap){
//        places_list = Place.getAll();

        List<LatLng> listCoordinates = new ArrayList<>();

        for(final Place p : places){

            MarkerOptions markerOptions = new MarkerOptions();

            LatLng latLng = new LatLng(p.getPlaceLat(), p.getPlaceLng());

            markerOptions.position(latLng);
            listCoordinates.add(latLng);

            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                markerOptions.title(p.getPlaceName());
            }
            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                markerOptions.title(p.getPlaceNameEn());
            }
//            если место было отмечено как пройденное, меняем иконку
            IconFactory iconFactory = IconFactory.getInstance(context);
            Icon icon;
            if(p.getPlaceWasOpen()){
                Drawable iconDrawable = ContextCompat.getDrawable(context, R.drawable.inactive_place);
                icon = iconFactory.fromDrawable(iconDrawable, 120, 120);
            } else{
                Drawable iconDrawable = ContextCompat.getDrawable(context, R.drawable.active_place);
                icon = iconFactory.fromDrawable(iconDrawable, 120, 120);
            }
            markerOptions.setIcon(icon);

            if(!p.getPlaceInvisible())
                map.addMarker(markerOptions);

//            настройка кастомного InfoWindow для маркера
            map.setInfoWindowAdapter(new MapboxMap.InfoWindowAdapter() {
                @Nullable
                @Override
                public View getInfoWindow(@NonNull final Marker marker) {

//                    централизуем камеру на выбранный маркер
                    Location loc = new Location(LocationManager.GPS_PROVIDER);
                    loc.setLatitude(marker.getPosition().getLatitude());
                    loc.setLongitude(marker.getPosition().getLongitude());

                    updateCameraPosition(loc, map.getCameraPosition().zoom);

                    final LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    infoWindow = inflater.inflate(R.layout.custom_info_window, null);

                    TextView info_title = (TextView) infoWindow.findViewById(R.id.title);
                    TypeFaceHelper.setDescriptionTypeFace(context, info_title);

                    Place place = Place.getByPlaceName(marker.getTitle());
                    if (place == null){
                        place = Place.getByPlaceNameEn(marker.getTitle());
                    }

                    if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                        info_title.setText(place.getPlaceName());
                    }
                    if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                        info_title.setText(place.getPlaceNameEn());
                    }

                    final Button button = (Button) infoWindow.findViewById(R.id.labelButton);
                    TypeFaceHelper.setDescriptionTypeFace(context, button);

                    button.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            если пользователь нажал на кнопку Отметить место как пройденное
//                            меняем иконку маркера и меняем значение флага в базе данных

                            Animation anim = AnimationUtils.loadAnimation(context, R.anim.info_window_anim);
                            infoWindow.startAnimation(anim);

                            Place place = null;

                            if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
                                place = Place.getByPlaceName(marker.getTitle());
                            }
                            if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
                                place = Place.getByPlaceNameEn(marker.getTitle());
                            }

//                            ставим флаг в таблице базы данных, указывающий на то, что место было пройдено
                            if(place != null){
                                IconFactory iconFactory = IconFactory.getInstance(context);
                                Drawable iconDrawable;
                                if(place.getPlaceWasOpen()){
                                    place.setPlaceWasOpen(false);
                                    button.setText(context.getResources().getString(R.string.place_was_open).toUpperCase());
                                    iconDrawable = ContextCompat.getDrawable(context
                                            , R.drawable.active_place);
                                } else {
                                    place.setPlaceWasOpen(true);
                                    button.setText(context.getResources().getString(R.string.place_was_close).toUpperCase());
                                    iconDrawable = ContextCompat.getDrawable(context
                                            , R.drawable.inactive_place);
                                }
                                place.save();

                                Icon icon = iconFactory.fromDrawable(iconDrawable, 120, 120);
                                marker.setIcon(icon);
                            } else {
                                Toast.makeText(context,
                                        context.getResources().getString(R.string.error_place_id),
                                        Toast.LENGTH_LONG).show();
                            }
                        }
                    });

                    if(p.getPlaceWasOpen())
                        button.setText(context.getResources().getString(R.string.place_was_close));
                    else
                        button.setText(context.getResources().getString(R.string.place_was_open));

                    ImageButton goToButton = (ImageButton)infoWindow.findViewById(R.id.goToButton);
                    goToButton.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
//                            если пользователь нажал на InfoWindow, передаем информацию о маркере в MarkerFragment

                            Intent intent = new Intent (activity, MarkerActivity.class);
                            intent.putExtra(Place.NAME, marker.getTitle());
                            activity.startActivity(intent);
                            activity.overridePendingTransition(R.anim.to_left_anim_start,R.anim.to_left_anim_end);
                        }
                    });

                    return infoWindow;
                }
            });
        }
        if(centralMap){
            LatLngBounds pointsOnMap = new LatLngBounds.Builder().includes(listCoordinates).build();
            map.moveCamera(CameraUpdateFactory.newLatLngBounds(pointsOnMap, 100));
            markersWasAdd = true;
        }
    }

//    убираем с карты все маркеры и маршруты
    public void mapClear(){
        map.clear();
    }

//    рисуем выбранный маршрут
    public void createRoute(int routeId){
        mapClear();

        List<PointsRoute> points = PointsRoute.getPointsRouteByParentId(routeId);

        List<Place> places = new ArrayList<>();
        for(PointsRoute p : points){
            places.add(Place.getByPlaceId(p.getPointsId()));
        }

        addMarkersOnMap(places, true);

        List<PolylineRoute> polyline = PolylineRoute.getRoutePolylineByParentId(routeId);

        List<LatLng> list = new ArrayList<>();
        LatLng[] pointCoordinates = new LatLng[polyline.size()];

        for (int i = 0; i < polyline.size(); i++) {
            LatLng latLng = new LatLng(
                    polyline.get(i).getRoutePolylineLat(),
                    polyline.get(i).getRoutePolylineLng());

            pointCoordinates[i] = latLng;
            list.add(latLng);

        }

        map.addPolyline(new PolylineOptions()
                .add(pointCoordinates)
                .color(Color.parseColor("#ff0000"))
                .width(5));

        helper.removeRouteId();
    }

    public void showInfoWindow(String title){
//        List<Marker> markers = map.getMarkers();
//
//        for(Marker m : markers){
//            if(m.getTitle().equals(title)){
//                m.showInfoWindow(map, mapView);
//                break;
//            }
//        }
//        helper.removePlaceLocation();
    }
    @Override
    public void onMyLocationChange(@Nullable Location location) {
//        если позиция пользователя изменилась, меняем положение камеры

        Log.d("myLog", "Позиция изменилась: " + location.getLatitude() + " " + location.getLongitude());
        helper.saveLocation(location.getLatitude(), location.getLongitude());

    }
}
