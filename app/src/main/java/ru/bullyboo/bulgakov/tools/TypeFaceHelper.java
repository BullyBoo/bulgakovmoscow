package ru.bullyboo.bulgakov.tools;

import android.content.Context;
import android.graphics.Typeface;
import android.widget.TextView;

/**
 * Класс-помощник, настраивающий шрифты TextView в приложении
 */
public class TypeFaceHelper {

    private static final String TITLE = "fonts/NewspaperSansC.otf";
    private static final String DESCRIPTION = "fonts/GTH75.otf";

    public static void setTitleTypeFace(Context context, TextView textView){
        if(textView != null){
            Typeface tf = Typeface.createFromAsset(context.getAssets(), TITLE);

            textView.setTypeface(tf);
        }
    }
    public static void setDescriptionTypeFace(Context context, TextView textView){
        if(textView != null){
            Typeface tf = Typeface.createFromAsset(context.getAssets(), DESCRIPTION);

            textView.setTypeface(tf);
        }
    }
}
