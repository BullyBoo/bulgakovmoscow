package ru.bullyboo.bulgakov.tools;

import android.content.Context;
import android.graphics.Color;
import android.util.Log;
import android.widget.Toast;

import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.annotations.PolylineOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.services.Constants;
import com.mapbox.services.commons.ServicesException;
import com.mapbox.services.commons.geojson.LineString;
import com.mapbox.services.commons.models.Position;
import com.mapbox.services.directions.v5.models.DirectionsRoute;
import com.mapbox.services.directions.v5.DirectionsCriteria;
import com.mapbox.services.directions.v5.MapboxDirections;
import com.mapbox.services.directions.v5.models.DirectionsResponse;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.data.place.Place;

/**
 * Данный класс-помощник, создает маршрут между двумя точками,
 * а так же отрисовывает его на карте
 */
public class RouteHelper {

    private Context context;

    private Place place;

    private MapboxMap map;

    private DirectionsRoute currentRoute;

    private PreferencesHelper helper;

    public RouteHelper(Context context , MapboxMap map, Place place){
        this.context = context;
        this.map = map;
        this.place = place;

        helper = new PreferencesHelper(context);
    }

    public void createRoute(double myLat, double myLng, double markerLat, double markerLng){

        map.clear();

        final Position origin = Position.fromCoordinates(myLng, myLat);

        final Position destination = Position.fromCoordinates(markerLng, markerLat);

        map.addMarker(new MarkerOptions()
                .position(new LatLng(origin.getLatitude(), origin.getLongitude()))
                .title("Вы здесь!"));

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU))
            map.addMarker(new MarkerOptions()
                .position(new LatLng(destination.getLatitude(), destination.getLongitude()))
                .title(place.getPlaceName())
                .snippet(place.getPlaceAddress()));

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN))
            map.addMarker(new MarkerOptions()
                    .position(new LatLng(destination.getLatitude(), destination.getLongitude()))
                    .title(place.getPlaceNameEn())
                    .snippet(place.getPlaceAddressEn()));

        try {
            getRoute(origin, destination);
        } catch (ServicesException e) {
            e.printStackTrace();
        }
    }

    private void getRoute(Position origin, Position destination) throws ServicesException {

        MapboxDirections client = new MapboxDirections.Builder()
                .setOrigin(origin)
                .setDestination(destination)
                .setProfile(DirectionsCriteria.PROFILE_CYCLING)
                .setAccessToken("pk.eyJ1IjoiYnVsbHlib28iLCJhIjoiY2lyaGdpdDhwMDAza2lkbWhpcDk0a29mZSJ9.MP_GCTdwNO_HqL7KtwvD4g")
                .build();

        client.enqueueCall(new Callback<DirectionsResponse>() {
            @Override
            public void onResponse(Call<DirectionsResponse> call, Response<DirectionsResponse> response) {
                Log.d("myLog", "Произошла ошибка: " + response.code());
                if (response.body() == null) {
                    Log.e("myLog", "Маршрут не найдет, возможно неправильно указан Access Tokken");
                    return;
                }

//                выводим немного информации о маршруте
                currentRoute = response.body().getRoutes().get(0);
                Log.d("myLog", "Distance: " + currentRoute.getDistance());
                Toast.makeText(context
                        ,context.getResources().getString(R.string.route_ready)
                                + currentRoute.getDistance()
                                + " "
                                + context.getResources().getString(R.string.meters)
                        , Toast.LENGTH_SHORT).show();

//                рисуем маршрут на карте
                drawRoute(currentRoute);
            }

            @Override
            public void onFailure(Call<DirectionsResponse> call, Throwable t) {
                Log.e("myLog", "Error: " + t.getMessage());
                Toast.makeText(context
                        , context.getResources().getString(R.string.route_error)
                                + t.getMessage()
                        , Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void drawRoute(DirectionsRoute route) {
//        конвертируем LineString в LatLng[]
        LineString lineString = LineString.fromPolyline(route.getGeometry(), Constants.OSRM_PRECISION_V5);
        List<Position> coordinates = lineString.getCoordinates();
        LatLng[] points = new LatLng[coordinates.size()];
        for (int i = 0; i < coordinates.size(); i++) {
            points[i] = new LatLng(
                    coordinates.get(i).getLatitude(),
                    coordinates.get(i).getLongitude());
        }
//        рисуем линии на карте
        map.addPolyline(new PolylineOptions()
                .add(points)
                .color(Color.parseColor("#009688"))
                .alpha(1)
                .width(5));
    }
}
