package ru.bullyboo.bulgakov.tools;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.Toast;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.asunctasks.PlaceLoader;
import ru.bullyboo.bulgakov.asunctasks.RouteLoader;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.PlaceJson;
import ru.bullyboo.bulgakov.data.route.RouteJson;
import ru.bullyboo.bulgakov.interfaces.DownloadDataState;
import ru.bullyboo.bulgakov.interfaces.FirstStartState;
import ru.bullyboo.bulgakov.ui.CustomDialog;
import ru.bullyboo.bulgakov.ui.FirstStartDialog;

/**
 * Данный класс отвечает за первоначальную настройку приложения
 * Первичная загрузка данных происходит именно из этого класса
 * Так же здесь проверяется наличие разрешений на получение геолокации, интернета и т д
 *
 * Данный класс запускается при каждом старте приложения, через интерфейс DownloadDataState
 * класс узнает о текущем состоянии загрузки, и что важнее, узнает, изменилась версия JSON или нет,
 * в зависимости от чего обновляет/не обновляет данные
 */
public class StartSettingsAppChecker implements DownloadDataState {

    private Context context;
    private FirstStartState state;

    private PreferencesHelper helper;

    private FirstStartDialog dialog;

    private PlaceLoader placeLoader;
    private RouteLoader routeLoader;

    private static final int PLACES_LOADER = 1;
    private static final int ROUTES_LOADER = 2;

    private static int tasksFinish = 0;

    public StartSettingsAppChecker(Context context, FirstStartState state, FirstStartDialog dialog){
        this.context = context;
        this.state = state;
        this.dialog = dialog;

        helper = new PreferencesHelper(context);

        haveInterentConnection();

        checkUpdate();

        state.getPermisson();
    }

//    проверяем наличие обновлений и существование базы данных
    private void checkUpdate(){
//        запускаем загрузчик
        placeLoader = new PlaceLoader(context, this, PLACES_LOADER);
        placeLoader.execute();

        routeLoader = new RouteLoader(context, this, ROUTES_LOADER);
        routeLoader.execute();
    }

//    проверяем был ли выбран язык по умолчанию
    private void checkLocal(){

        if(!helper.hasLocal()) {
            showLocalDialog();
        }else{
            state.firstSettingWasEnd();
        }

    }

    private void showLocalDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

            builder.setTitle(context.getResources().getString(R.string.dialog_local_titel))
                    .setMessage(context.getResources().getString(R.string.dialog_local_message));

            builder.setPositiveButton(context.getResources().getString(R.string.ru_local)
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            helper.saveLocal(PreferencesHelper.LOCAL_RU);
                            helper.saveFirstStartApp();

                            state.firstSettingWasEnd();

                            StartSettingsAppChecker
                                    .this.dialog.setCheckBox6Text(context
                                            .getResources()
                                            .getString(R.string.check_box61));

                            StartSettingsAppChecker.this.dialog.setCheckBox6Checked(true);
                            StartSettingsAppChecker.this.dialog.setProgreeBar6Visible(false);
                        }
                    });
            builder.setNegativeButton(context.getResources().getString(R.string.en_local)
                    , new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            helper.saveLocal(PreferencesHelper.LOCAL_EN);
                            helper.saveFirstStartApp();

                            state.firstSettingWasEnd();

                            StartSettingsAppChecker
                                    .this.dialog.setCheckBox6Text(context
                                    .getResources()
                                    .getString(R.string.check_box61));

                            StartSettingsAppChecker.this.dialog.setCheckBox6Checked(true);
                            StartSettingsAppChecker.this.dialog.setProgreeBar6Visible(false);
                        }
                    });
            builder.setCancelable(false);

            AlertDialog alertDialog = builder.create();
            alertDialog.show();
    }

//    методы интерфейса загрузки данных
    @Override
    public void successfullDownload(int task_id) {
        stopAsunc(task_id);
        tasksFinish++;

        if(task_id == PLACES_LOADER){
            dialog.setCheckBox2Text(context.getResources().getString(R.string.check_box23));
            dialog.setCheckBox2Checked(true);
            dialog.setProgreeBar2Visible(false);
        } else if( task_id == ROUTES_LOADER){
            dialog.setCheckBox3Text(context.getResources().getString(R.string.check_box33));
            dialog.setCheckBox3Checked(true);
            dialog.setProgreeBar3Visible(false);
        }

        if(tasksFinish == ROUTES_LOADER){
            checkLocal();
        }
    }

    @Override
    public void jsonVersionWasNotChange(int task_id) {
        stopAsunc(task_id);
        tasksFinish++;

        if(task_id == PLACES_LOADER){
            dialog.setCheckBox2Text(context.getResources().getString(R.string.check_box21));
            dialog.setCheckBox2Checked(true);
            dialog.setProgreeBar2Visible(false);
        } else if( task_id == ROUTES_LOADER){
            dialog.setCheckBox3Text(context.getResources().getString(R.string.check_box31));
            dialog.setCheckBox3Checked(true);
            dialog.setProgreeBar3Visible(false);
        }

        if(tasksFinish == ROUTES_LOADER){
            checkLocal();
        }
    }

    @Override
    public void jsonVersionWasChange(int task_id) {
        if(task_id == PLACES_LOADER){
            dialog.setCheckBox2Text(context.getResources().getString(R.string.check_box22));
            dialog.setCheckBox2Checked(true);
            dialog.setProgreeBar2Visible(false);
        } else if( task_id == ROUTES_LOADER){
            dialog.setCheckBox3Text(context.getResources().getString(R.string.check_box32));
            dialog.setCheckBox3Checked(true);
            dialog.setProgreeBar3Visible(false);
        }
    }

    @Override
    public void errorDownload(Exception e, int task_id) {
        stopAsunc(task_id);

        if(task_id == PLACES_LOADER){
            dialog.setCheckBox2Text(context.getResources().getString(R.string.check_box24));
            dialog.setProgreeBar2Visible(false);
        } else if( task_id == ROUTES_LOADER){
            dialog.setCheckBox3Text(context.getResources().getString(R.string.check_box34));
            dialog.setProgreeBar3Visible(false);
        }

        if(helper.hasFirstStartApp()){
            tasksFinish++;
            if (task_id == PLACES_LOADER)
                dialog.setCheckBox2Checked(true);
            else if(task_id == ROUTES_LOADER)
                dialog.setCheckBox3Checked(true);

            if (tasksFinish == ROUTES_LOADER)
                checkLocal();

        } else{

            if(task_id == PLACES_LOADER){
                PlaceJson placeJsonInfo = PlaceJson.getPlaceJsonInfo();
                placeJsonInfo.delete();

                placeLoader = new PlaceLoader(context, this, PLACES_LOADER);
                placeLoader.execute();

            } else if(task_id == ROUTES_LOADER){
                RouteJson routeJsonInfo = RouteJson.getRouteJsonInfo();
                routeJsonInfo.delete();

                routeLoader = new RouteLoader(context, this, ROUTES_LOADER);
                routeLoader.execute();
            }
        }
    }

    @Override
    public void noInterentConnection(int task_id) {
        stopAsunc(PLACES_LOADER);
        stopAsunc(ROUTES_LOADER);

        dialog.setCheckBox1Text(context.getResources().getString(R.string.check_box12));
        dialog.setProgreeBar1Visible(false);

        if(helper.hasFirstStartApp()){
            checkLocal();
        }else{
            Toast.makeText(context,
                    context.getResources().getString(R.string.no_internet_connection_first_start),
                    Toast.LENGTH_LONG).show();

        }
    }

    private void haveInterentConnection() {
        CheckInternetConnection checkInternetConnection = new CheckInternetConnection(context);
        if(checkInternetConnection.isOnline()){
            dialog.setCheckBox1Text(context.getResources().getString(R.string.check_box11));
            dialog.setCheckBox1Checked(true);
            dialog.setProgreeBar1Visible(false);
        }
    }

    //    останавливаем поток, для предотвращения утечки памяти
    public void stopAsunc(int task_id){
        if(task_id == PLACES_LOADER)
            if(placeLoader != null)
                placeLoader.cancel(true);
        else if(task_id == ROUTES_LOADER)
                if(routeLoader != null)
                    routeLoader.cancel(true);
    }

}
