package ru.bullyboo.bulgakov.tools;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.util.Log;

import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import java.util.List;

import ru.bullyboo.bulgakov.R;
import ru.bullyboo.bulgakov.adapters.ViewPagerAdapter;
import ru.bullyboo.bulgakov.data.place.PicturesPlace;
import ru.bullyboo.bulgakov.data.place.Place;
import ru.bullyboo.bulgakov.data.place.TagsPlace;

/**
 * Данный класс является универсальным шаблоном, для кнопки "Поделиться".
 * Он подключается к базе данных и берет необходимые параметры для создания поста.
 * После того, как шаблон был создан и заполнен, он передается диалоговому окну,
 * прописанном в facebook sdk. Вся дальнейшая работа остается за ним.
 *
 * Само выполнение авторизации пользователя, а так же реализации действия "Поделиться"
 * полностью ложиться на facebook sdk
 *
 */
public class ShareHelper {

    private PreferencesHelper helper;
    private Context context;
    private Activity activity;

    private ShareDialog shareDialog;

    private String placeName;
    private List<PicturesPlace> picturesList;
    private List<TagsPlace> tagsList;

    public ShareHelper(Context context, Activity activity){
        this.context = context;
        helper = new PreferencesHelper(context);

        shareDialog = new ShareDialog(activity);
    }

//    в данном методе создается и заполняется шаблон поста
    public ShareLinkContent createShared(Place p){

        if(helper.getLocal().equals(PreferencesHelper.LOCAL_RU)){
            placeName = p.getPlaceName();
        } else if(helper.getLocal().equals(PreferencesHelper.LOCAL_EN)){
            placeName = p.getPlaceNameEn();
        }

        picturesList = PicturesPlace.getByParentId(p.getPlaceId());

        tagsList = TagsPlace.getByMainId(p.getPlaceId());

        ShareLinkContent.Builder linkBuilder = new ShareLinkContent.Builder();

        Log.d("myLog", "placeName = " + placeName);
        if(placeName != null)
            linkBuilder.setContentTitle(placeName + context.getResources().getString(R.string.share_title));

        linkBuilder.setContentDescription(context.getResources().getString(R.string.share_desc));

        if(picturesList != null){
            linkBuilder.setImageUrl(Uri.parse(ViewPagerAdapter.MAIN_URL + picturesList.get(0).getPicturesUrl()));
        }

        linkBuilder.setContentUrl(Uri.parse("http://moscow.bulgakovmuseum.ru/"));

        ShareLinkContent content = linkBuilder.build();

        return content;
    }
//    данный метод передает этот шаблон диалоговому окну встроенному в facebook sdk
    public void sendShare(ShareLinkContent content){

        shareDialog.show(content);
    }
}
