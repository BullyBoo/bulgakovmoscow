package ru.bullyboo.bulgakov.tools;

/**
 * Класс-помощник
 * Высчитывает расстояние в метрах между двумя географическими координатами
 */
public class DistanceHelper {

    public static double distance(double lat1_, double lat2_, double lon1_,
                                  double lon2_) {
        double R = 6368500.0; // в метрах

        double lat1 = deg2rad(lat1_);
        double lon1 = deg2rad(lon1_);
        double lat2 = deg2rad(lat2_);
        double lon2 = deg2rad(lon2_);

        double distance = Math.acos(Math.sin(lat1) * Math.sin(lat2)
                + Math.cos(lat1) * Math.cos(lat2) * Math.cos(lon2 - lon1))
                * R;

        return distance;

    }

    private static double deg2rad(double deg) {
        return (deg * Math.PI / 180.0);
    }
}
