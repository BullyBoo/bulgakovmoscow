package ru.bullyboo.bulgakov.interfaces;

import java.util.List;

import ru.bullyboo.bulgakov.data.place.Place;

/**
 * Данный интерфейс помогает отследить состояние подсчета дистанции
 * от геопозиции пользователя до каждого маркера.
 * В силу того, что данный подсчет занимает большое количество памяти,
 * он был вынесен в отдельный поток
 */
public interface CalculateDistanceState {

//    приложение никогда не получало геолокации пользователя
    void neverHadLocation();

//    дистанция до маркеров было подсчитана
    void distanceWasCount(List<Place> list);
}
