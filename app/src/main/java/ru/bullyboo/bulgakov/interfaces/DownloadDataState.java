package ru.bullyboo.bulgakov.interfaces;

/**
 * Реализация данного интерфейса помогает отслеживать состояние загрузчика,
 * в соответствии с которым приложение выполняет конкретные действия
 */
public interface DownloadDataState {

//    загрузка прошла успешно
    void successfullDownload(int task_id);

//    версия Json не изменилась
    void jsonVersionWasNotChange(int task_id);

//    версия Json изменилась
    void jsonVersionWasChange(int task_id);

//    произошла ошибка при загрузке
    void errorDownload(Exception e, int task_id);

//    отсутствует соединение с интернетом
    void noInterentConnection(int task_id);

}
